-- RELATORIO DE RESPOSTAS 
SELECT 
    d.nome AS dimensoes,
    ca.nome AS categorias,
    i.nome AS indicadores,
    a.nome AS aspectos,
    a.pergunta,
    usu.usuario,
    -- ,
    -- (select criterio.descricao from criterio where criterio.id = r.criterio_id) as  respostas,
    r.criterio_id
FROM
    usuario usu,
    criterio c
        JOIN
    aspecto a ON a.id = c.aspecto_id
        JOIN
    indicador i ON i.id = a.indicador_id
        JOIN
    categoria ca ON ca.id = i.categoria_id
        JOIN
    dimensao d ON d.id = ca.dimensao_id
        JOIN
    universo u ON u.id = d.universo_id
        JOIN
    avaliacao ava ON ava.universo_id = u.id
        LEFT JOIN
    resposta r ON r.criterio_id = c.id
WHERE
    ava.id = 22
        AND usu.id IN (SELECT 
            usu.id
        FROM
            usuario usu
                INNER JOIN
            resposta r ON r.usuario_id = usu.id
                INNER JOIN
            aspecto a ON a.id = r.aspecto_id
                INNER JOIN
            indicador i ON i.id = a.indicador_id
                INNER JOIN
            categoria c ON c.id = i.categoria_id
                INNER JOIN
            dimensao d ON d.id = c.dimensao_id
                INNER JOIN
            universo u ON u.id = d.universo_id
                INNER JOIN
            amostra amo ON amo.universo_id = u.id
                INNER JOIN
            avaliacao ava ON ava.amostra_id = amo.id
        WHERE
            ava.id = 22
        GROUP BY usu.id)
GROUP BY usu.id , a.pergunta
ORDER BY usu.usuario, d.id , ca.id , i.id , a.nome;





