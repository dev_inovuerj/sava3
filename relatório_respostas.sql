/* Relatório de Respostas*/
SELECT 
    ava.titulo AS avaliacao,
    usu.id AS usuario_id,
    a.pergunta AS aspecto,
    CONCAT('[', usu.usuario, '] RESPONDEU : ') AS usuario
    ,
    (SELECT 
            criterio.descricao
        FROM
            criterio
        WHERE
            id = r.criterio_id) AS RESPOSTA
FROM
    resposta r
        LEFT JOIN
    aspecto a ON a.id = r.aspecto_id
        LEFT JOIN
    indicador i ON i.id = a.indicador_id
        LEFT JOIN
    categoria c ON c.id = i.categoria_id
        LEFT JOIN
    dimensao d ON d.id = c.dimensao_id
        LEFT JOIN
    universo u ON u.id = d.universo_id
        LEFT JOIN
    amostra amo ON amo.universo_id = u.id
        LEFT JOIN
    avaliacao ava ON ava.amostra_id = amo.id
        LEFT JOIN
    usuario usu ON usu.id = r.usuario_id
WHERE
    ava.id = 22
ORDER BY usu.id , d.id , c.id , i.id , a.nome;



/* RELATÓRIO ESTRUTURAL DE UMA AVALIAÇÃO */
SELECT 
    ava.titulo AS avaliacao,
    d.nome as dimensao,
    u.nome as universo,
    c.nome as categoria,
    i.id as indicador_id,
    -- i.nome as indicador,
    a.pergunta AS aspecto
FROM
    aspecto a
        INNER JOIN
    indicador i ON i.id = a.indicador_id
        INNER JOIN
    categoria c ON c.id = i.categoria_id
        INNER JOIN
    dimensao d ON d.id = c.dimensao_id
        INNER JOIN
    universo u ON u.id = d.universo_id
        INNER JOIN
    amostra amo ON amo.universo_id = u.id
        INNER JOIN
    avaliacao ava ON ava.amostra_id = amo.id
WHERE
    ava.id = 22
ORDER BY d.id , c.id , i.id , a.nome;



/* Perguntas e Respostas de um usuário */
select 
    a.pergunta as aspecto , usu.usuario, c.descricao as criterio
from
    resposta r
	inner join aspecto a on a.id = r.aspecto_id
        inner join
    usuario usu ON usu.id = r.usuario_id
		inner join
	criterio c on c.id = r.criterio_id
where
    r.id = 3016;
