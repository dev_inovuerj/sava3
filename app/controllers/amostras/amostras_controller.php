<?php
class AmostrasController extends AppController {
	
	var $name = "Amostras";
	
	function index($universoId){
		$this->layout = "logado";
		$this->paginate = array("separator"=>"/", 'order' => array('Amostra.nome' => 'asc'), "limit" => 5, 'conditions' => array('Amostra.universo_id' => $universoId));
		
		//Armazena o id do universo para ser usado em outros m�todos da clase amostra
		$this->Session->write('Universo.id',$universoId);	
		
		$this->set("amostras",$this->paginate("Amostra"));
	}
	
	function cadastrar(){
		$universoId = $this->Session->read('Universo.id');
		$this->layout = "logado";
		if(!empty($this->data)) {
				$this->data['Amostra']['universo_id'] = $universoId;
				if($this->Amostra->save($this->data)) {
            				// define uma mensagem de flash na sess�o e redireciona.
            				$this->Session->setFlash("Amostra cadastrada com sucesso!","success");
							$this->Session->write("Amostra.id",$this->Amostra->id);
            				$this->redirect("/avaliacaos/cadastrar/");
        		}else{
					$this->Session->setFlash("Ocorreu um problema ao cadastrar a sua amostra. Tente novamente mais tarde!","error");
					$this->redirect("/amostras/cadastrar/");
				}
		}
	}
	
	function editar($id = null){
		$this->layout = "logado";
		$idAvaliacao = $this->Session->read('Avaliacao.id');
		$avaliacao = $this->Amostra->Universo->Objeto->Avaliacao->find('first',array('conditions' => array('Avaliacao.id' => $idAvaliacao)));
		$this->Amostra->id = $avaliacao['Amostra']['id'];
		if(empty($this->data)) {
			$this->data = $this->Amostra->read();
		}else{
			if($this->Amostra->save($this->data)) {
				// define uma mensagem de flash na sess�o e redireciona.
				$this->Session->setFlash("Amostra editada com sucesso!","success");
				$this->Session->delete('Avaliacao.id');
				$this->redirect("/avaliacaos/");
			}else{
				$this->Session->setFlash("Ocorreu um problema ao editar a sua amostra. Tente novamente mais tarde!","error");
				$this->Session->delete('Avaliacao.id');
				$this->redirect("/avaliacaos/");
			}
		}
	}
	
	function excluir($id){
		$this->data = $this->Amostra->read();
		$this->Amostra->delete($id);
		$this->Session->setFlash('Amostra exclu�da com sucesso!','success');
		$this->redirect('/amostras/index/'.$this->data['Amostra']['universo_id']);
	}
	
	function escolher(){
		$this->layout = "logado";
		$universoId = $this->Session->read('Avaliacao.universoId');
		$todosAmostras = $this->Amostra->find('all', array('conditions'=>array('Amostra.universo_id'=>$universoId)));
		if(!empty($todosAmostras)){
			foreach($todosAmostras as $indice => $valor){
				$amostra[$valor['Amostra']['id']]= $valor['Amostra']['qtd_limitante'].' '.$valor['Amostra']['nome'];
			}
			$this->set("amostra", $amostra);
		}else{
			$this->Session->setFlash("O universo escolhido n�o possui nenhuma amostra!","error");
			$this->redirect("/universos/escolher/");
		}
		if(!empty($this->data)) {
			$this->Session->write('Avaliacao.amostraId',$this->data['Amostra']['amostra_id']);
			$this->redirect("/avaliacaos/cadastrar/");
		}
	}

}
?>