<?php

class CategoriasController extends AppController {

    var $name = "Categorias";
    var $permissions = array(
        'getId' => array('Visitante'),
        'getCategoria' => array('Administrador', 'SysAdmin')
    );

    function index($id) {
        $this->layout = "logado";

        $dimensao = $this->Categoria->Dimensao->find('first', array('conditions' => array('Dimensao.id' => $id)));
        $this->Session->write('Dimensao.nome', $dimensao['Dimensao']['nome']);

        $this->Session->write('Dimensao.id', $id);
        $this->paginate = array("separator" => "/", 'order' => array('Categoria.nome' => 'asc'), "limit" => 5, 'conditions' => array('Categoria.dimensao_id' => $id));
        $this->set("categorias", $this->paginate("Categoria"));
    }

    function cadastrar() {
        $dimensaoId = $this->Session->read('Dimensao.id');
        $this->layout = "logado";
        if (!empty($this->data)) {
            $this->data['Categoria']['dimensao_id'] = $dimensaoId;
            if ($this->Categoria->save($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                $this->Session->setFlash("Categoria cadastrada com sucesso!", "success");
                $this->Session->delete('Universo.id');
                $this->redirect("/categorias/index/" . $dimensaoId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao cadastrar a sua categoria. Tente novamente mais tarde!", "error");
                $this->Session->delete('Universo.id');
                $this->redirect("/categorias/" . $dimensaoId);
            }
        }
    }

    function editar($id = null) {
        $this->layout = "logado";
        $this->Categoria->id = $id;
        if (empty($this->data)) {
            $this->data = $this->Categoria->read();
            $this->Session->write('Dimensao.id', $this->data['Dimensao']['id']);
        } else {
            $dimensaoId = $this->Session->read('Dimensao.id');
            if ($this->Categoria->save($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                $this->Session->setFlash("Categoria editada com sucesso!", "success");
                $this->Session->delete('Dimensao.id');
                $this->redirect("/categorias/index/" . $dimensaoId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao editar a sua categoria. Tente novamente mais tarde!", "error");
                $this->Session->delete('Dimensao.id');
                $this->redirect("/categorias/index/" . $dimensaoId);
            }
        }
    }

    function excluir($id) {
        $this->data = $this->Categoria->read();
        $this->Categoria->delete($id);
        $this->Session->setFlash('Categoria exclu�da com sucesso!', 'success');
        $this->redirect('/categorias/index/' . $this->data['Dimensao']['id']);
    }

    function getId() {
        foreach ($this->Session->read('Dimensao') as $indice => $value) {
            $categoriaNaoFormatado[] = $this->Categoria->find('all', array('conditions' => array('Categoria.dimensao_id' => $value)));
        }
        foreach ($categoriaNaoFormatado as $indice => $value) {
            foreach ($value as $i => $v) {
                $categoria[] = $v['Categoria']['id'];
            }
        }
        $this->Session->delete('Dimensao');
        $this->Session->write('Categoria', $categoria);
    }

    function getCategoria() {
        if (is_array($this->Session->read('Dimensao'))) {
            foreach ($this->Session->read('Dimensao') as $indice => $value) {
                $categoriaNaoFormatado[] = $this->Categoria->find('all', array('conditions' => array('Categoria.dimensao_id' => $value), 'recursive' => -1));
            }
        } else {
            $categoriaNaoFormatado[] = $this->Categoria->find('all', array('conditions' => array('Categoria.dimensao_id' => $this->Session->read('Dimensao')), 'recursive' => -1));
        }

        $categoria = array();
        foreach ($categoriaNaoFormatado as $indice => $value) {
            foreach ($value as $i => $v) {
                $categoria[] = $v;
            }
        }

        $this->Session->delete('Dimensao');
        $this->Session->write('Categoria', $categoria);
    }

}