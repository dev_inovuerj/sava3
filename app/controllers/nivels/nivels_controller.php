<?php
class NivelsController extends AppController {
	
	var $name = "Nivels";
	var $nivel = '';
	var $permissions = array(
		'getNivel' => array('Administrador', 'SysAdmin')
	);
	
	function listar($tipo_nivel_id){
		$nivel = $this->Nivel->find('all',array('conditions'=>array('tipo_nivel_id' => $tipo_nivel_id),'order'=>'Nivel.grau'));
		foreach($nivel as $indice => $valor){
			$this->nivel[]['id']= $valor['Nivel']['id'];
		}
		return $this->nivel;
	}
	
	function getNivel($id){
		$nivel = $this->Nivel->find('first',array('conditions' => array('Nivel.id' => $id)));
		return $nivel['Nivel']['nivel'];
	}
}
?>