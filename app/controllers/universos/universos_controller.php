<?php

class UniversosController extends AppController {

    var $name = "Universos";

    function index($objetoId) {
        $this->layout = "logado";
        $this->paginate = array("separator" => "/", 'order' => array('Universo.nome' => 'asc'), "limit" => 5, 'conditions' => array('Universo.objeto_id' => $objetoId));

        //Armazena o id do objeto para ser usado em outros m�todos da clase universo
        $this->Session->write('Objeto.id', $objetoId);

        $this->set("universos", $this->paginate("Universo"));
    }

    function cadastrar() {
        $objetoId = $this->Session->read('Objeto.id');
        // echo $this->Session->read('Objeto.id');
        $this->layout = "logado";
        if (!empty($this->data)) {
            $this->data['Universo']['objeto_id'] = $objetoId;
            if ($this->Universo->save($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                $this->Session->setFlash("Universo cadastrado com sucesso!", "success");
                $this->Session->write("Universo.id", $this->Universo->id);
                $this->redirect("/amostras/cadastrar/");
            } else {
                $this->Session->setFlash("Ocorreu um problema ao cadastrar o seu universo. Tente novamente mais tarde!", "error");
                $this->redirect("/universos/cadastrar/");
            }
        }
    }

    function editar($id = null) {
        $this->layout = "logado";
        $idAvaliacao = $this->Session->read('Avaliacao.id');
        $avaliacao = $this->Universo->Objeto->Avaliacao->find('first', array('conditions' => array('Avaliacao.id' => $idAvaliacao)));
        $this->Universo->id = $avaliacao['Universo']['id'];
        if (empty($this->data)) {
            $this->data = $this->Universo->read();
        } else {
            if ($this->Universo->save($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                $this->Session->setFlash("Universo editado com sucesso!", "success");
                $this->Session->delete('Avaliacao.id');
                $this->redirect("/avaliacaos/");
            } else {
                $this->Session->setFlash("Ocorreu um problema ao editar o seu universo. Tente novamente mais tarde!", "error");
                $this->Session->delete('Avaliacao.id');
                $this->redirect("/avaliacaos/");
            }
        }
    }

    function excluir($id) {
        $this->data = $this->Universo->read();
        $this->Universo->delete($id);
        $this->Session->setFlash('Universo exclu�do com sucesso!', 'success');
        $this->redirect('/universos/index/' . $this->data['Universo']['objeto_id']);
    }

    function escolher() {
        $this->layout = "logado";
        $objetoId = $this->Session->read('Avaliacao.objetoId');
        $todosUniversos = $this->Universo->find('all', array('conditions' => array('Universo.objeto_id' => $objetoId)));
        if (!empty($todosUniversos)) {
            foreach ($todosUniversos as $indice => $valor) {
                $universo[$valor['Universo']['id']] = $valor['Universo']['nome'];
            }
            $this->set("universo", $universo);
        } else {
            $this->Session->setFlash("O objeto escolhido n�o possui nenhum universo!", "error");
            $this->redirect("/objetos/escolher/");
        }
        if (!empty($this->data)) {
            $this->Session->write('Avaliacao.universoId', $this->data['Universo']['universo_id']);
            $this->redirect("/amostras/escolher/");
        }
    }

}

?>