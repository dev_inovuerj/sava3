<?php

class AspectosController extends AppController {

    var $name = "Aspectos";
    var $permissions = array(
        'responder' => array('Visitante'),
        'getId' => array('Visitante'),
        'getAspecto' => array('Administrador', 'SysAdmin')
    );

    function index($id) {
        $this->layout = "logado";

        $indicador = $this->Aspecto->Indicador->find('first', array('conditions' => array('Indicador.id' => $id)));
        $this->Session->write('Indicador.nome', $indicador['Indicador']['nome']);
        $this->Session->write('Indicador.id', $id);
        $this->paginate = array("separator" => "/", 'order' => array('Aspecto.nome' => 'asc'), "limit" => 5, 'conditions' => array('Aspecto.indicador_id' => $id));
        $this->set("aspectos", $this->paginate("Aspecto"));
    }

    function cadastrar() {
        $indicadorId = $this->Session->read('Indicador.id');
        $this->set("tipo_nivel", $this->requestAction('/tipo_nivels/listar'));
        $this->layout = "logado";
        if (!empty($this->data)) {
            $this->data['Aspecto']['indicador_id'] = $indicadorId;
            $nivel = $this->requestAction('/nivels/listar/' . $this->data['Aspecto']['tipo_nivel_id']);
            for ($i = 0; $i < 4; $i++) {
                $this->data['Criterio'][$i]['tipo_nivel_id'] = $this->data['Aspecto']['tipo_nivel_id'];
                $this->data['Criterio'][$i]['nivel_id'] = $nivel[$i]['id'];
            }
            if ($this->Aspecto->saveAll($this->data)) {
                // define uma mensagem de flash na sess?o e redireciona.
                $this->Session->setFlash("Aspecto cadastrado com sucesso!", "success");
                $this->Session->delete('Indicador.id');
                $this->redirect("/aspectos/index/" . $indicadorId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao cadastrar o seu aspecto. Tente novamente mais tarde!", "error");
                $this->Session->delete('Indicador.id');
                $this->redirect("/aspectos/" . $indicadorId);
            }
        }
    }

    function getAspecto() {


        /* Se houver mais de um indicadores */
        if (is_array($this->Session->read('Indicador'))) {
            foreach ($this->Session->read('Indicador') as $indice => $value) {
                $aspectoNaoFormatado[] = $this->Aspecto->find('all', array('conditions' => array('Aspecto.indicador_id' => $value)));
            }
        } else {
            /* Apenas um indicador */
            $aspectoNaoFormatado[] = $this->Aspecto->find('all', array('conditions' => array('Aspecto.indicador_id' => $this->Session->read('Indicador')), 'recursive' => -1));
        }

        $aspecto = array();
        foreach ($aspectoNaoFormatado as $indice => $value) {
            foreach ($value as $i => $v) {
                $aspecto[] = $v;
            }
        }



        $this->Session->delete('Indicador');
        $this->Session->write('Aspecto', $aspecto);
    }

    function responder() {

        $this->layout = "avaliador_logado";

        /* Os Modelos abaixo s�o criados na sess�o */
        $this->requestAction('/dimensaos/getId/');
        $this->requestAction('/categorias/getId/');
        $this->requestAction('/indicadors/getId/');

        /* Retorna Aspecto para sess�o */
        $this->getAspecto();

        # Configura v�riaveis para serem transmitidas para as views 
        $this->set('aspecto', $this->Session->read('Aspecto'));



        # POST - Ap�s submit de respostas 
        if (!empty($this->data)) {

            $i = 0;
            foreach ($this->data['Pergunta'] as $indice => $value) {
                $resposta[$i]['aspecto_id'] = $indice;
                $resposta[$i]['criterio_id'] = $value;
                $resposta[$i]['usuario_id'] = $this->Session->read('Auth.Usuario.id');
                $i++;
            }
            # Grava na sess�o cada resposta de um aspecto(pergunta)
            $this->Session->write('Resposta', $resposta);

            # Grava��o das respostas na tabela(resposta)
            $this->requestAction('/respostas/cadastrar/');
        }
    }

    function editar($id = null) {
        $this->layout = "avaliador_logado";
        $this->set("tipo_nivel", $this->requestAction('/tipo_nivels/listar'));
        $this->Aspecto->id = $id;
        if (empty($this->data)) {
            $this->data = $this->Aspecto->read();
            $this->Session->write('Indicador.id', $this->data['Indicador']['id']);
        } else {
            $indicadorId = $this->Session->read('Indicador.id');
            $this->data['Aspecto']['indicador_id'] = $indicadorId;
            $nivel = $this->requestAction('/nivels/listar/' . $this->data['Aspecto']['tipo_nivel_id']);

            $criterioId = $this->Aspecto->Criterio->find('list', array('conditions' => array('Criterio.aspecto_id' => $id)));
            $criterioId = array_values($criterioId);



            for ($i = 0; $i < 4; $i++) {
                $this->data['Criterio'][$i]['id'] = $criterioId[$i];
                $this->data['Criterio'][$i]['tipo_nivel_id'] = $this->data['Aspecto']['tipo_nivel_id'];
                $this->data['Criterio'][$i]['nivel_id'] = $nivel[$i]['id'];
            }
            $this->data['Aspecto']['id'] = $id;


            if ($this->Aspecto->saveAll($this->data)) {
                // define uma mensagem de flash na sess?o e redireciona.
                $this->Session->setFlash("Aspecto editado com sucesso!", "success");
                $this->Session->delete('Indicador.id');
                $this->redirect("/aspectos/index/" . $indicadorId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao editar o seu aspecto. Tente novamente mais tarde!", "error");
                $this->Session->delete('Indicador.id');
                $this->redirect("/aspectos/index/" . $indicadorId);
            }
        }
    }

    function excluir($id) {

        $indicadorId = $this->Session->read('Indicador.id');

        $this->Aspecto->Resposta->deleteAll(array('Resposta.aspecto_id' => $id), false);
        $this->Aspecto->Criterio->deleteAll(array('Criterio.aspecto_id' => $id), false);

        if ($this->Aspecto->delete($id)) {
            $this->Session->setFlash('Aspecto exclu�do com sucesso!', 'success');
        } else {
            $this->Session->setFlash('Erro!', 'error');
        }

        $this->redirect("/aspectos/index/" . $indicadorId);
    }

}
