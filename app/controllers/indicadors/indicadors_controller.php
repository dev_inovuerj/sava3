<?php

class IndicadorsController extends AppController {

    var $name = "Indicadors";
    var $permissions = array(
        'getId' => array('Visitante'),
        'getIndicador' => array('Administrador', 'SysAdmin')
    );

    function index($id) {
        $this->layout = "logado";

        $categoria = $this->Indicador->Categoria->find('first', array('conditions' => array('Categoria.id' => $id)));
        $this->Session->write('Categoria.nome', $categoria['Categoria']['nome']);

        $this->Session->write('Categoria.id', $id);
        $this->paginate = array("separator" => "/", 'order' => array('Indicador.nome' => 'asc'), "limit" => 5, 'conditions' => array('Indicador.categoria_id' => $id));
        $this->set("indicadors", $this->paginate("Indicador"));
    }

    function cadastrar() {
        $categoriaId = $this->Session->read('Categoria.id');
        $this->layout = "logado";
        if (!empty($this->data)) {
            $this->data['Indicador']['categoria_id'] = $categoriaId;
            if ($this->Indicador->save($this->data)) {
                // define uma mensagem de flash na sess„o e redireciona.
                $this->Session->setFlash("Indicador cadastrado com sucesso!", "success");
                $this->Session->delete('Categoria.id');
                $this->redirect("/indicadors/index/" . $categoriaId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao cadastrar o seu indicador. Tente novamente mais tarde!", "error");
                $this->Session->delete('Categoria.id');
                $this->redirect("/indicadors/" . $categoriaId);
            }
        }
    }

    function editar($id = null) {
        $this->layout = "logado";
        $this->Indicador->id = $id;
        if (empty($this->data)) {
            $this->data = $this->Indicador->read();
            $this->Session->write('Categoria.id', $this->data['Categoria']['id']);
        } else {
            $categoriaId = $this->Session->read('Categoria.id');
            if ($this->Indicador->save($this->data)) {
                // define uma mensagem de flash na sess„o e redireciona.
                $this->Session->setFlash("Indicador editado com sucesso!", "success");
                $this->Session->delete('Categoria.id');
                $this->redirect("/indicadors/index/" . $categoriaId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao editar o seu Indicador. Tente novamente mais tarde!", "error");
                $this->Session->delete('Categoria.id');
                $this->redirect("/indicadors/index/" . $categoriaId);
            }
        }
    }

    function excluir($id) {
        $this->data = $this->Indicador->read();
        $this->Indicador->delete(intval($id));
        $this->Session->setFlash('Indicador excluÌdo com sucesso!', 'success');
        $this->redirect('/indicadors/index/' . $this->data['Categoria']['id']);
    }

    function getId() {
        foreach ($this->Session->read('Categoria') as $indice => $value) {
            $indicadorNaoFormatado[] = $this->Indicador->find('all', array('conditions' => array('Indicador.categoria_id' => $value)));
        }
        foreach ($indicadorNaoFormatado as $indice => $value) {
            foreach ($value as $i => $v) {
                $indicador[] = $v['Indicador']['id'];
            }
        }
        $this->Session->delete('Categoria');
        $this->Session->write('Indicador', $indicador);
    }

    function getIndicador() {
        if (is_array($this->Session->read('Categoria'))) {
            foreach ($this->Session->read('Categoria') as $indice => $value) {
                $indicadorNaoFormatado[] = $this->Indicador->find('all', array('conditions' => array('Indicador.categoria_id' => $value), 'recursive' => -1));
            }
        } else {
            $indicadorNaoFormatado[] = $this->Indicador->find('all', array('conditions' => array('Indicador.categoria_id' => $this->Session->read('Categoria')), 'recursive' => -1));
        }
        $indicador = array();
        foreach ($indicadorNaoFormatado as $indice => $value) {
            foreach ($value as $i => $v) {
                $indicador[] = $v;
            }
        }

        $this->Session->delete('Categoria');
        $this->Session->write('Indicador', $indicador);
    }

}

?>