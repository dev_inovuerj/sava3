<?php

class DimensaosController extends AppController {

    var $name = "Dimensaos";
    var $permissions = array(
        'getId' => array('Visitante'),
        'getDimensao' => array('Administrador', 'SysAdmin')
    );

    function index($id) {
        $this->layout = "logado";
        
        # Guardando nome da avalia��o (Se for primeiro acesso a avalia��o guardar na sess�o)
        $this->Session->write('Avaliacao.titulo', $this->Dimensao->Universo->Avaliacao->field('titulo', array('Avaliacao.universo_id' => $id)));
        # $avaliacao = $this->Dimensao->Universo->Avaliacao->findByUniverso_id($this->Session->read('Universo.id'));
        # $this->Session->delete('Avaliacao.titulo');

        $universo = $this->Dimensao->Universo->find('first', array('conditions' => array('Universo.id' => $id)));
        $this->Session->write('Universo.nome', $universo['Universo']['nome']);

        $this->Session->write('Universo.id', $id);
        $this->paginate = array("separator" => "/", 'order' => array('Dimensao.nome' => 'asc'), "limit" => 5, 'conditions' => array('Dimensao.universo_id' => $id));
        $this->set("dimensaos", $this->paginate("Dimensao"));
    }

    function cadastrar() {
        $universoId = $this->Session->read('Universo.id');
        $this->layout = "logado";
        if (!empty($this->data)) {
            $this->data['Dimensao']['universo_id'] = $universoId;
            if ($this->Dimensao->save($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                $this->Session->setFlash("Dimens�o cadastrada com sucesso!", "success");
                $this->Session->delete('Universo.id');
                $this->redirect("/dimensaos/index/" . $universoId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao cadastrar a sua dimens�o. Tente novamente mais tarde!", "error");
                $this->Session->delete('Universo.id');
                $this->redirect("/dimensaos/" . $universoId);
            }
        }
    }

    function editar($id = null) {
        $this->layout = "logado";
        $this->Dimensao->id = $id;
        if (empty($this->data)) {
            $this->data = $this->Dimensao->read();
            $this->Session->write('Universo.id', $this->data['Universo']['id']);
        } else {
            $universoId = $this->Session->read('Universo.id');
            if ($this->Dimensao->save($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                $this->Session->setFlash("Dimens�o editada com sucesso!", "success");
                $this->Session->delete('Universo.id');
                $this->redirect("/dimensaos/index/" . $universoId);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao editar a sua dimens�o. Tente novamente mais tarde!", "error");
                $this->Session->delete('Universo.id');
                $this->redirect("/dimensaos/index/" . $universoId);
            }
        }
    }

    function excluir($id) {
        $this->data = $this->Dimensao->read();
        $this->Dimensao->delete($id);
        $this->Session->setFlash('Dimens�o exclu�da com sucesso!', 'success');
        $this->redirect('/dimensaos/index/' . $this->data['Universo']['id']);
    }

    function getId() {
        $dimensaoNaoFormatado = $this->Dimensao->find('all', array('conditions' => array('Dimensao.universo_id' => $this->Session->read('Universo.id'))));
        foreach ($dimensaoNaoFormatado as $indice => $value) {
            $dimensao[] = $value['Dimensao']['id'];
        }
        //$this->Session->delete('Universo.id');
        $this->Session->write('Dimensao', $dimensao);
    }

    function getDimensao() {

        foreach ($this->Session->read('Universo') as $indice => $value) {
            $dimensaoNaoFormatado[] = $this->Dimensao->find('all', array('conditions' => array('Dimensao.universo_id' => $value), 'recursive' => -1));
        }

        foreach ($dimensaoNaoFormatado as $indice => $value) {
            foreach ($value as $i => $v) {
                $dimensao[] = $v;
            }
        }



        $this->Session->delete('Universo');
        $this->Session->write('Dimensao', $dimensao);
    }

}
