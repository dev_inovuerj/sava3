<?php

class AvaliacaosController extends AppController {

    var $name = "Avaliacaos";
    var $helpers = array('Html', 'Session', 'Javascript', 'Form', 'Paginator', 'GoogleChart');
    var $tipoAvaliacao = array();
    var $tipoAbordagem = array();
    var $tipoResultado = array();
    var $objeto = array();
    var $permissions = array(
        'escolher' => array('Visitante'),
        'index' => array('Administrador', 'SysAdmin'),
        'emitirRelatorio' => array('Administrador', 'SysAdmin'),
        'emitirRelatorioEstrutural' => '*'
    );

    function index() {



        if ($this->Session->read('SysAdmin') == true) {
            $this->layout = "avaliador_logado";
            $this->paginate = array("separator" => "/", "order" => array("Avaliacao.responsavel" => "asc"), "limit" => 30);
            $this->set("avaliacaos", $this->paginate("Avaliacao"));
            $this->render('index_sysadmin');
        } else {
            $this->layout = "logado";

            # Se for o administrador ? pode ver todas as avalia��es 
            $conditions = ( $this->Session->read('Auth.Usuario.id') == 1 ) ? array() : array("Avaliacao.proprietario_id" => $this->Session->read('Auth.Usuario.id'));

            # Parametros da pagina��o
            $this->paginate = array(
                "separator" => "/",
                'order' => array('Avaliacao.responsavel' => 'asc'),
                "limit" => 30,
                "conditions" => $conditions
            );
            

            
            $this->set("avaliacaos", $this->paginate("Avaliacao"));
        }
    }

    function cadastrar() {

        $this->layout = "logado";

        //Busca os tipos de avalia��o no banco e disponibilza um array para a view
        $tipoAvaliacao = $this->Avaliacao->TipoAvaliacao->find('all');
        foreach ($tipoAvaliacao as $indice => $valor) {
            $this->tipoAvaliacao[$valor['TipoAvaliacao']['id']] = $valor['TipoAvaliacao']['tipo'];
        }
        $this->set("tipoAvaliacao", $this->tipoAvaliacao);

        //Busca os tipos de abordagem no banco e disponibilza um array para a view
        $tipoAbordagem = $this->Avaliacao->TipoAbordagem->find('all');
        foreach ($tipoAbordagem as $indice => $valor) {
            $this->tipoAbordagem[$valor['TipoAbordagem']['id']] = $valor['TipoAbordagem']['tipo'];
        }
        $this->set("tipoAbordagem", $this->tipoAbordagem);

        //Busca os tipos de resultado no banco e disponibilza um array para a view
        $tipoResultado = $this->Avaliacao->TipoResultado->find('all');
        foreach ($tipoResultado as $indice => $valor) {
            $this->tipoResultado[$valor['TipoResultado']['id']] = $valor['TipoResultado']['tipo'];
        }
        $this->set("tipoResultado", $this->tipoResultado);

        if (!empty($this->data)) {
            $this->data['Avaliacao']['objeto_id'] = $this->Session->read('Objeto.id');
            $this->data['Avaliacao']['universo_id'] = $this->Session->read('Universo.id');
            $this->data['Avaliacao']['amostra_id'] = $this->Session->read('Amostra.id');
            $this->data['Avaliacao']['proprietario_id'] = $this->Session->read('Auth.Usuario.id');
            if ($this->Avaliacao->save($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                $this->Session->delete('Avaliacao');
                $this->Session->setFlash("Avalia��o cadastrada com sucesso!", "success");
                $this->redirect("/avaliacaos/");
            } else {
                $this->Session->setFlash("Ocorreu um problema ao cadastrar a sua avalia��o. Tente novamente mais tarde!", "error");
                $this->Session->delete('Avaliacao');
                $this->redirect("/avaliacaos/");
            }
        }
    }

    function escolher() {

        $this->layout = "avaliador_logado";
        $options['joins'] = array(
            array('table' => 'avaliacao_usuario',
                'alias' => 'AvaliacaosUsuario',
                'type' => 'inner',
                'conditions' => array(
                    'Avaliacao.id = AvaliacaosUsuario.avaliacao_id'
                )
            ),
            array('table' => 'usuario',
                'alias' => 'Usuario',
                'type' => 'inner',
                'conditions' => array(
                    'AvaliacaosUsuario.usuario_id = Usuario.id'
                )
            )
        );
        $options['conditions'] = array(
            'Usuario.id' => $this->Session->read('Auth.Usuario.id')
        );

        # POST
        if (!empty($this->data)) {

            /* Recuperou id do usuario logado */
            $idUsuario = $this->Session->read('Auth.Usuario.id');
            /* Persistindo para recuperar/localizar avalia??o por c?digo */
            $avaliacao = $this->Avaliacao->find('first', array('conditions' => array('Avaliacao.codigo' => $this->data['Avaliacao']['codigo'])));

            /* Cria??o de variaveis de sess?o Universo[id] & Avaliacao[id] */
            $this->Session->write('Universo.id', $avaliacao['Avaliacao']['universo_id']);
            $this->Session->write('Avaliacao.id', $avaliacao['Avaliacao']['id']);

            $idAvaliacao = $this->Session->read('Avaliacao.id');

            /* Verifica se j? consta avalia??o deste usu?rio */
            $jaavaliou = $this->Avaliacao->AvaliacaoUsuario->find('first', array("conditions" => array("avaliacao_id" => $idAvaliacao, "usuario_id" => $idUsuario), "recursive" => -1));



            /* Se retornar alguma avalia??o */
            if (!empty($avaliacao)) {
                /* E , se est? ainda n?o tiver sido respondida, redireciono para responder */
                if (!$jaavaliou) {
                    $this->redirect("/aspectos/responder/");
                } else {
                    $this->Session->setFlash("Voc&ecirc; j&aacute; respondeu este question&aacute;rio.", "success");
                }
            } else {
                $this->set('msg', "<p>Question&aacute;rio n&atilde;o encontrado.</p>");
            }
        }

        $this->set('respondido', isset($jaavaliou) && count($jaavaliou) > 0);
    }

    function emitirRelatorio($id) {

        # Layout View utilizado
        $this->layout = "logado";

        # Resgatar �nica avalia��o por id
        $avaliacao = $this->Avaliacao->find('first', array('conditions' => array('Avaliacao.id' => $id)));

        # Seta vari�vel com avalia��o para view emitirRelatorio
        $this->set('avaliacao', $avaliacao);

        # Fornece id do universo da avalia��o para Sess�o
        $this->Session->write('Universo.id', $avaliacao['Avaliacao']['universo_id']);

        # Executa M�todo/Action getDimensao do Controller dimensaos
        $this->requestAction('/dimensaos/getDimensao/');

        # Recupera array com dimens�es da sess�o criada pelo action getDimensao
        $relatorio = $this->Session->read('Dimensao');

        # Iniciando de dimens�o itera sobre Categorias/Indicadores/Aspectos/Respostas
        $contadorDimensao = 0;
        foreach ($relatorio as $indice => $value) {
            #  Gravo Dimensao na sess�o para o que m�todo getCategoria visualize-o
            $this->Session->write('Dimensao', $value['Dimensao']['id']);
            # Crio sess�o array com categorias desta dimens�o
            $this->requestAction('/categorias/getCategoria/');
            # Array de categorias � inserido no relat�rio
            $relatorio[$contadorDimensao]['Categorias'] = $this->Session->read('Categoria');

            $contadorCategoria = 0;
            foreach ($relatorio[$contadorDimensao]['Categorias'] as $indice => $value) {
                $this->Session->write('Categoria', $value['Categoria']['id']);
                $this->requestAction('/indicadors/getIndicador/');
                $relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'] = $this->Session->read('Indicador');

                $contadorIndicador = 0;
                foreach ($relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'] as $indice => $value) {
                    $this->Session->write('Indicador', $value['Indicador']['id']);
                    $this->requestAction('/aspectos/getAspecto/');
                    $relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'][$contadorIndicador]['Aspectos'] = $this->Session->read('Aspecto');

                    $contadorAspecto = 0;
                    foreach ($relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'][$contadorIndicador]['Aspectos'] as $indice => $value) {
                        $this->Session->write('Aspecto', $value['Aspecto']['id']);
                        $this->requestAction('/respostas/getResposta');
                        $relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'][$contadorIndicador]['Aspectos'][$contadorAspecto]['Respostas'] = $this->Session->read('Resposta');

                        $contadorAspecto++;
                    }
                    $contadorIndicador++;
                }
                $contadorCategoria++;
            }
            $contadorDimensao++;
        }

        $this->set('relatorio', $relatorio);
        if ($this->Session->read('SysAdmin') == true) {
            $this->layout = 'avaliador_logado';
        }
    }

    function emitirRelatorioEstrutural($id) {

        # Layout View utilizado
        $this->layout = "relatorio";

        # Resgatar �nica avalia��o por id
        $avaliacao = $this->Avaliacao->find('first', array('conditions' => array('Avaliacao.id' => $id)));

        # Seta vari�vel com avalia��o para view emitirRelatorio
        $this->set('avaliacao', $avaliacao);

        # Fornece id do universo da avalia��o para Sess�o
        $this->Session->write('Universo.id', $avaliacao['Avaliacao']['universo_id']);

        # Executa M�todo getDimensao do Controller dimensaos
        $this->requestAction('/dimensaos/getDimensao/');

        # Recupera array com dimens�es da sess�o criada pelo action getDimensao
        $relatorio = $this->Session->read('Dimensao');

        # Iniciando de dimens�o itera sobre Categorias/Indicadores/Aspectos/Respostas
        $contadorDimensao = 0;
        foreach ($relatorio as $indice => $value) {
            #  Gravo Dimensao na sess�o para o que m�todo getCategoria visualize-o
            $this->Session->write('Dimensao', $value['Dimensao']['id']);
            # Crio sess�o array com categorias desta dimens�o
            $this->requestAction('/categorias/getCategoria/');
            # Array de categorias � inserido no relat�rio
            $relatorio[$contadorDimensao]['Categorias'] = $this->Session->read('Categoria');

            $contadorCategoria = 0;
            foreach ($relatorio[$contadorDimensao]['Categorias'] as $indice => $value) {
                $this->Session->write('Categoria', $value['Categoria']['id']);
                $this->requestAction('/indicadors/getIndicador/');
                $relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'] = $this->Session->read('Indicador');

                $contadorIndicador = 0;
                foreach ($relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'] as $indice => $value) {
                    $this->Session->write('Indicador', $value['Indicador']['id']);
                    $this->requestAction('/aspectos/getAspecto/');
                    $relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'][$contadorIndicador]['Aspectos'] = $this->Session->read('Aspecto');

                    $contadorAspecto = 0;
                    foreach ($relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'][$contadorIndicador]['Aspectos'] as $indice => $value) {
                        $this->Session->write('Aspecto', $value['Aspecto']['id']);
                        $this->requestAction('/respostas/getResposta');
                        $relatorio[$contadorDimensao]['Categorias'][$contadorCategoria]['Indicadores'][$contadorIndicador]['Aspectos'][$contadorAspecto]['Respostas'] = $this->Session->read('Resposta');

                        $contadorAspecto++;
                    }
                    $contadorIndicador++;
                }
                $contadorCategoria++;
            }
            $contadorDimensao++;
        }

        $this->set('relatorio', $relatorio);
        if ($this->Session->read('SysAdmin') == true) {
            $this->layout = 'avaliador_logado';
        }



//        debug($avaliacao);
//        pr($avaliacao);
//        pr($relatorio);
//        exit;
    }

    function associar($idAvaliacao, $idUsuario = null) {
        if (!$idUsuario) {
            $this->layout = "logado";
            $usuarioAssociado = array(); //inicializa��o da vari�vel que ir� armazenar os usu�rios j� associados a avalia��o
            $idUsuario = $this->Session->read('Auth.Usuario.id');
            $usuario = $this->Avaliacao->Usuario->find('all', array("conditions" => array("criado_por" => $idUsuario), "recursive" => -1));
            $avaliacaoUsuario = $this->Avaliacao->AvaliacaoUsuario->find('all', array("conditions" => array("avaliacao_id" => $idAvaliacao), "recursive" => -1));
            foreach ($avaliacaoUsuario as $indice => $valor) {
                $usuarioAssociado[] = $this->Avaliacao->Usuario->find('first', array("conditions" => array("id" => $valor['AvaliacaoUsuario']['usuario_id']), "recursive" => -1));
            }
            $this->set('usuario', $usuario);
            $this->set('idUsuario', $idUsuario);
            $this->set('usuarioAssociado', $usuarioAssociado);
            $this->set('idAvaliacao', $idAvaliacao);
        } else {
            $a = array();
            $b = array();
            foreach ($this->data as $indice => $valor) {
                foreach ($valor as $indice => $valor) {
                    foreach ($valor as $indice => $valor) {
                        if ($valor) {
                            $a["AvaliacaoUsuario"]["usuario_id"] = $valor;
                            $a["AvaliacaoUsuario"]["avaliacao_id"] = $idAvaliacao;
                            $b[] = $a;
                        }
                    }
                }
            }
            $this->data = $b;
            if ($this->Avaliacao->AvaliacaoUsuario->saveAll($this->data)) {
                // define uma mensagem de flash na sess�o e redireciona.
                //debug($this->data);
                $this->Session->setFlash("Usu�rio associado com sucesso!", "success");
                $this->redirect("/avaliacaos/associar/" . $idAvaliacao);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao associar o usu�rio a avalia��o. Tente novamente mais tarde!", "error");
                $this->Session->delete('Avaliacao');
                $this->redirect("/avaliacaos/");
            }
        }
    }

    function editar($idAvaliacao) {
        $this->layout = "logado";

        $this->Session->write('Avaliacao.id', $idAvaliacao);
    }

    function excluir($id) {
        $this->data = $this->Avaliacao->read();
        $this->Avaliacao->delete($id);
        $this->Session->setFlash('Avaliacao exclu�da com sucesso!', 'success');
        $this->redirect('/avaliacaos/');
    }

    function editar_cabecalho() {
        $this->layout = "logado";
        $idAvaliacao = $this->Session->read('Avaliacao.id');

        //Busca os tipos de avalia��o no banco e disponibilza um array para a view
        $tipoAvaliacao = $this->Avaliacao->TipoAvaliacao->find('all');
        foreach ($tipoAvaliacao as $indice => $valor) {
            $this->tipoAvaliacao[$valor['TipoAvaliacao']['id']] = $valor['TipoAvaliacao']['tipo'];
        }
        $this->set("tipoAvaliacao", $this->tipoAvaliacao);

        //Busca os tipos de abordagem no banco e disponibilza um array para a view
        $tipoAbordagem = $this->Avaliacao->TipoAbordagem->find('all');
        foreach ($tipoAbordagem as $indice => $valor) {
            $this->tipoAbordagem[$valor['TipoAbordagem']['id']] = $valor['TipoAbordagem']['tipo'];
        }
        $this->set("tipoAbordagem", $this->tipoAbordagem);

        //Busca os tipos de resultado no banco e disponibilza um array para a view
        $tipoResultado = $this->Avaliacao->TipoResultado->find('all');
        foreach ($tipoResultado as $indice => $valor) {
            $this->tipoResultado[$valor['TipoResultado']['id']] = $valor['TipoResultado']['tipo'];
        }
        $this->set("tipoResultado", $this->tipoResultado);

        $this->Avaliacao->id = $idAvaliacao;

        if (empty($this->data)) {
            $this->data = $this->Avaliacao->read();
        } else {
            $this->data['Avaliacao']['id'] = $idAvaliacao;
            if ($this->Avaliacao->save($this->data)) {
                $this->Session->setFlash("Cabe�alho da avalia��o editado com sucesso!", "success");
                $this->Session->delete('Avaliacao.id');
                $this->redirect("/avaliacaos/editar/" . $idAvaliacao);
            } else {
                $this->Session->setFlash("Ocorreu um problema ao editar o cabe�alho da avalia��o. Tente novamente mais tarde!", "error");
                $this->Session->delete('Avaliacao.id');
                $this->redirect("/avaliacaos/editar/" . $idAvaliacao);
            }
        }
    }

    function beforeFilter() {
        parent::beforeFilter();
        
        $this->Auth->allow('emitirRelatorioEstrutural');
    }

}

/* function escolher() {
      $this->layout = "avaliador_logado";
      $options['joins'] = array(
      array('table' => 'avaliacao_usuario',
      'alias' => 'AvaliacaosUsuario',
      'type'  => 'inner',
      'conditions' =>  array(
      'Avaliacao.id = AvaliacaosUsuario.avaliacao_id'
      )
      ),
      array('table' => 'usuario',
      'alias' => 'Usuario',
      'type'  => 'inner',
      'conditions' => array(
      'AvaliacaosUsuario.usuario_id = Usuario.id'
      )
      )
      );
      $options['conditions'] = array(
      'Usuario.id' => $this->Session->read('Auth.Usuario.id')
      );
      $todosAvaliacaos = $this->Avaliacao->find('all',$options);
      foreach($todosAvaliacaos as $indice => $valor){
      $avaliacao[$valor['Avaliacao']['id']]= $valor['Avaliacao']['titulo'];
      }
      $this->set('avaliacaos',$avaliacao);

      if(!empty($this->data)) {
      $avaliacao = $this->Avaliacao->find('first',array('conditions' => array('Avaliacao.id'=>$this->data['Avaliacao']['avaliacao_id'])));
      $this->Session->write('Universo.id',$avaliacao['Avaliacao']['universo_id']);
      $this->redirect("/aspectos/responder/");
      }
      } */