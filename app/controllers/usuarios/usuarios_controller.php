<?php

class UsuariosController extends AppController
{

    var $name = 'Usuarios';
    var $permissions = array(
        'index' => 'Administrador',
        'logout' => '*',
        'registrar' => '*',
        'teste' => '*'
    );

    function login()
    {
        $this->pageTitle = 'Login';


        if ($this->Auth->user()) {


            $this->Session->write('Auth.Usuario.grupo', $this->Usuario->Grupo->field('nome', array('id' => $this->Auth->user('grupo_id'))));


            if ($this->Session->read('Auth.Usuario.grupo') == 'Administrador') {
                $this->Session->write('SysAdmin', false);


                /* Redireciona para URL especificada. Tratamento para logar indo diretamente para URL informada. */
                if ($this->Session->read('URL_SOLICITADA')) {
                    $this->Auth->loginRedirect = array(
                        'controller' => $this->Session->read('URL_SOLICITADA.controlador'),
                        'action' => $this->Session->read('URL_SOLICITADA.acao'),
                        $this->Session->read('URL_SOLICITADA.valorParametro')
                    );
                } else {
                    $this->Auth->loginRedirect = array('controller' => 'ajudas', 'action' => 'index');
                }


            } else if ($this->Session->read('Auth.Usuario.grupo') == 'Visitante') {
                $this->Auth->loginRedirect = array('controller' => 'avaliacaos', 'action' => 'escolher');
            } else if ($this->Session->read('Auth.Usuario.grupo') == 'SysAdmin') {

                $this->Session->write('SysAdmin', true);
                $this->Auth->loginRedirect = array('controller' => 'avaliacaos', 'action' => 'index');
            }

            $this->redirect($this->Auth->redirect());
        }
    }

    function logout()
    {
        $this->Session->delete('URL_SOLICITADA');
        $this->redirect($this->Auth->logout());
    }

    function index()
    {
        $this->layout = "logado";
    }

    function cadastrar()
    {
        $this->layout = "logado";
        if (!empty($this->data)) {
            $this->data['Usuario']['criado_por'] = $this->Session->read('Auth.Usuario.id');
            $this->data['Usuario']['grupo_id'] = 2;
            if ($this->Usuario->save($this->data)) {
                // define uma mensagem de flash na sess???o e redireciona.
                $this->Session->setFlash("Usuario cadastrado com sucesso!", "success");
                $this->redirect("/usuarios/listar/");
            } else {
                $this->Session->setFlash("Ocorreu um problema ao cadastrar o seu usu??rio. Tente novamente mais tarde!", "error");
                $this->redirect("/usuarios/listar/");
            }
        }
    }

    function listar()
    {
        $this->layout = "logado";
        $this->paginate = array("separator" => "/", 'order' => array('usuario' => 'asc'), "limit" => 5, 'conditions' => array('Usuario.criado_por' => $this->Session->read('Auth.Usuario.id')));
        $this->set("usuarios", $this->paginate("Usuario"));
    }

    function registrar()
    {
        $this->layout = "registro";
        if (!empty($this->data)) {
            $retorno = $this->Usuario->find('first', array('conditions' => array('Usuario.usuario' => $this->data['Usuario']['usuario']), 'recursive' => -1));
            if (!empty($retorno)) {
                $this->Session->setFlash("O nome de usu&aacute;rio escolhido n&atilde;o est&aacute; dispon&iacute;vel. Por favor escolha outro.", "error");
            } else {
                $this->data['Usuario']['grupo_id'] = 2;
                if ($this->Usuario->save($this->data)) {
                    // define uma mensagem de flash na sess???o e redireciona.
                    $this->Session->setFlash("Usuario cadastrado com sucesso!", "success");
                    $this->redirect("/usuarios/login/");
                } else {
                    $this->Session->setFlash("Ocorreu um problema ao cadastrar o seu usu??rio. Tente novamente mais tarde!", "error");
                    $this->redirect("/usuarios/login/");
                }
            }
        }
    }

    function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('registrar', 'login');
    }

}
