<?php
class TipoNivelsController extends AppController {
	
	var $name = "TipoNivels";
	var $tipo_nivel = '';
	
	function listar(){
		$tipo_nivel = $this->TipoNivel->find('all');
		foreach($tipo_nivel as $indice => $valor){
			$this->tipo_nivel[$valor['TipoNivel']['id']]= $valor['TipoNivel']['tipo'];
		}
		return $this->tipo_nivel;
	}
	
}
?>