<?php
class ObjetosController extends AppController {

	var $name = "Objetos";

	
	function index(){
		$this->layout = "logado";
		$this->paginate = array("separator"=>"/", 'order' => array('nome' => 'asc'), "limit" => 5, 'conditions' => array('Objeto.usuario_id'=>$this->Session->read('Auth.Usuario.id')));
		$this->set("objetos",$this->paginate("Objeto"));
		
	}
	
	function cadastrar(){
		$this->layout = "logado";
		if(!empty($this->data)) {
				$this->data['Objeto']['usuario_id'] = $this->Session->read('Auth.Usuario.id');
				if($this->Objeto->save($this->data)) {
            				// define uma mensagem de flash na sess�o e redireciona.
            				$this->Session->setFlash("Objeto cadastrado com sucesso!","success");
							// echo $this->Objeto->id;
							$this->Session->write("Objeto.id",$this->Objeto->id);
            				$this->redirect("/universos/cadastrar/");
        		}else{
					$this->Session->setFlash("Ocorreu um problema ao cadastrar o seu objeto. Tente novamente mais tarde!","error");
					$this->redirect("/objetos/cadastrar/");
				}
		}
	}
	
	function editar($id = null){
		$this->layout = "logado";
		$idAvaliacao = $this->Session->read('Avaliacao.id');
		$avaliacao = $this->Objeto->Avaliacao->find('first',array('conditions' => array('Avaliacao.id' => $idAvaliacao)));
		$this->Objeto->id = $avaliacao['Objeto']['id'];
		if(empty($this->data)) {
			$this->data = $this->Objeto->read();
		}else{
			$this->data['Objeto']['usuario_id'] = $this->Session->read('Auth.Usuario.id');
			if($this->Objeto->save($this->data)) {
						// define uma mensagem de flash na sess�o e redireciona.
						$this->Session->setFlash("Objeto editado com sucesso!","success");
						$this->Session->delete('Avaliacao.id');
						$this->redirect("/avaliacaos/");
			}else{
				$this->Session->setFlash("Ocorreu um problema ao editar o seu objeto. Tente novamente mais tarde!","error");
				$this->Session->delete('Avaliacao.id');
				$this->redirect("/avaliacaos/");
			}
		}
	}
	
	function excluir($id){
		$this->Objeto->delete($id);
		$this->Session->setFlash('Objeto exclu�do com sucesso!','success');
		$this->redirect('/objetos/');
	}
	
	function escolher(){
		$this->layout = "logado";
		$todosObjetos = $this->Objeto->find('all',array('conditions'=>array('Objeto.usuario_id'=>$this->Session->read('Auth.Usuario.id'))));
		if(!empty($todosObjetos)){
			foreach($todosObjetos as $indice => $valor){
				$objeto[$valor['Objeto']['id']]= $valor['Objeto']['nome'];
			}
			$this->set("objeto", $objeto);
		}else{
			$this->Session->setFlash("N�o h� nenhum objeto cadastrado atualmente!","error");
			$this->redirect("/avaliacaos/");
		}
		
		if(!empty($this->data)) {
			$this->Session->write('Avaliacao.objetoId',$this->data['Objeto']['objeto_id']);
			$this->redirect("/universos/escolher/");
		}
	}
	
}
?>