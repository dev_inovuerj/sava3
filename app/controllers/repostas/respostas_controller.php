<?php

class RespostasController extends AppController {

    var $name = 'Respostas';
    var $permissions = array(
        'cadastrar' => array('Visitante'),
        'getResposta' => array('Administrador', 'SysAdmin')
    );

    function cadastrar() {

        $resposta = $this->Session->read('Resposta');
        $idUsuario = $this->Session->read('Auth.Usuario.id');
        $idAvaliacao = $this->Session->read('Avaliacao.id');
        $this->loadModel('Avaliacao');

        $jaavaliou = $this->Avaliacao->AvaliacaoUsuario->find('first', array("conditions" => array("avaliacao_id" => $idAvaliacao, "usuario_id" => $idUsuario), "recursive" => -1));

        # Se j� avaliou exibo mensagem 
        if ($jaavaliou) {
            $this->Session->setFlash("Voc&ecirc; j&aacute; respondeu este question&aacute;rio.", "success");
        } else {
            if ($this->Resposta->saveAll($resposta)) {
                $this->Avaliacao->AvaliacaoUsuario->create();
                $this->Avaliacao->AvaliacaoUsuario->save(array("AvaliacaoUsuario" => array("avaliacao_id" => $idAvaliacao, "usuario_id" => $idUsuario)));
                // define uma mensagem de flash na sess„o e redireciona.
                $this->Session->setFlash("Obrigado por responder o question&aacute;rio!", "success");
            } else {
                
            }
        }
        $this->Session->delete('Resposta');
        $this->redirect("/avaliacaos/escolher/");
    }

    function getResposta() {
        $resposta['total_respostas'] = $this->Resposta->find('count', array('conditions' => array('Resposta.aspecto_id' => $this->Session->read('Aspecto'))));
        $criterioId = $this->Resposta->Criterio->find('list', array('conditions' => array('Criterio.aspecto_id' => $this->Session->read('Aspecto'))));
        $j = 0;
        foreach ($criterioId as $indice => $value) {
            $criterio[$j]['total'] = $this->Resposta->find('count', array('conditions' => array('Resposta.criterio_id' => $value)));
            $criterioNaoFormatado = $this->Resposta->Criterio->find('first', array('conditions' => array('Criterio.id' => $value), 'recursive' => -1));
            $criterio[$j]['Criterio'] = $criterioNaoFormatado['Criterio'];
            $criterio[$j]['Nivel'] = $this->requestAction('/nivels/getNivel/' . $criterioNaoFormatado['Criterio']['nivel_id']);
            $j++;
        }
        $resposta['criterios'] = $criterio;
        $this->Session->write('Resposta', $resposta);
    }

}
