<h3>A Metodologia A3</h3>
<br />
        A avalia��o em todo o seu processo apresenta uma necessidade dupla. Se por um lado, cada vez mais se utilizam os resultados da avalia��o como indicadores de mudan�a, por outro lado, cada vez mais � preciso mudar para realizar os processos de avalia��o. Tal manifesta��o revela a urg�ncia de se estudar o momento do processo avaliativo, o que significa responder a quest�o � qual o ambiente de aplica��o da avalia��o? A Metodologia A3 � uma proposta modelo para a cria��o de um ambiente de condi��es, em que se aplique o processo avaliativo com mais possibilidades de sucesso, considerando conhecer o momento da avalia��o na organiza��o, o ambiente, a cultura instalada, os personagens envolvidos, bem como as necessidades, desejos de cada se��o e setor da organiza��o, al�m da identifica��o dos objetivos expressos da avalia��o, o perfil dos avaliados e avaliadores.
<br /><br />

<h3>O Modelo</h3>
<br />
        Com vistas a responder esta quest�o, buscamos construir um modelo metodol�gico que d� conta de identificar o momento do processo avaliativo e o ambiente institucional vigente, de modo que, os resultados da avalia��o realizada possam, efetivamente, diagnosticar situa��es e indicar as interven��es necess�rias e poss�veis de mudan�as e aprimoramentos.
<br /><br />
        A avalia��o tem na defini��o de seus objetivos a fun��o de identificar como o processo de avalia��o dever� acontecer, com a defini��o da abordagem, e como os resultados dever�o ser apresentados, com a defini��o do car�ter. Alguns objetivos podem ter uma �nica abordagem e permitir a apresenta��o de seus resultados em mais de um tipo de car�ter. Uma abordagem estrat�gica para identificar a visibilidade de uma institui��o ou programa pode apresentar resultados de car�ter social e pol�tico. Elaborar uma proposta de avalia��o de programa que atenda o ambiente e cultura da institui��o, promova o conhecimento das a��es e benef�cios do programa e que tenha total participa��o dos envolvidos � tarefa complexa, perseguida por institui��es e estudiosos no tema. <br /><br />

<h3>Princ�pios B�sicos da A3</h3>
<br />
        Princ�pio 1 � Ser uma proposta metodol�gica de orienta��o e apoio ao processo avaliativo.<br />
        Princ�pio 2 � Apresentar avalia��o na dimens�o de aprimoramento e transforma��o A Metodologia A3 objetiva contribuir para os resultados do processo de avalia��o de quem deseja realizar avalia��o sob o paradigma de transformar, crescer e aprimorar.<br /><br />

<h3>As Fases de Desenvolvimento da Metodologia A3 s�o:</h3>
<br /><br />
<strong>Fase 1:</strong> Defini��o dos pressupostos do processo avaliativo.
<br /><br />
        - Para qu� avaliar<br />
        - A quem cabe avaliar<br />
        - A quem � dirigida a avalia��o<br />
        - Quais as v�timas da avalia��o<br /><br />

<strong>Fase 2:</strong> Identifica��o dos fatores de relev�ncia para o ambiente de aplica��o da avalia��o.
<br /><br />
        - Ambiente e momento<br />
        - Agente de transforma��o<br />
        - Crit�rio de avalia��o da produ��o<br />
        - Salvaguarda<br /><br />

<strong>Fase 3:</strong> Identifica��o dos fatores cr�ticos para o ambiente de aplica��o da avalia��o
<br /><br />
        Os quatro fatores cr�ticos para a constru��o do ambiente de aplica��o da avalia��o que, de acordo com o formato da avalia��o, poder�o ser desdobrados.
<br /><br />
        a) Planejamento e Estrutura��o
<br /><br />

        � um elemento estrutural dos mais importantes, considerando-se inclusive a participa��o de todos. Um plano de a��o com a defini��o dos personagens envolvidos se caracteriza como fase cr�tica de todo o projeto e principalmente quanto � observ�ncia da adequa��o �s realidades e necessidades da institui��o. Importante conhecer como conscientizar a comunidade da nova abordagem avaliativa, qual o perfil do avaliador mais condizente com o corpo t�cnico a ser investigado, que instrumentos poder�o ser mais efetivos e, finalmente, como orientar a divulga��o dos resultados e promover as transforma��es sugeridas.
<br /><br />
        Devem constar do planejamento:
<br /><br />

        � A defini��o do plano de apresenta��o, divulga��o dos resultados e acompanhamentos; <br />
        � Constitui-se base para esta fase o conhecimento da cultura instalada e o momento proposto para a avalia��o. <br /><br />

        b) Justificativa do processo avaliativo
<br /><br />

        A apresenta��o � comunidade e divulga��o do plano de avalia��o tem o car�ter de justificativa. � de extrema import�ncia sensibilizar a comunidade avaliada da vis�o de constru��o e transforma��o, sem traumas, castigos ou penalidades. Os objetivos de aprimorar e crescer devem ser exacerbados e disseminados. Todo o processo avaliativo deve ser apresentado. A comunidade institucional precisa conhecer as vantagens de se avaliar. A proposta deve ser adequada ao p�blico a que se destina. A elabora��o de documentos de promo��o da proposta � necess�ria. Folders, cartazes e manuais com divulga��o clara do projeto, objetivos, como e quando ser� realizado, bem como a proje��o dos benef�cios e aprimoramentos dever� ser preparada e distribu�da.
<br /><br />
        c) Acompanhamento durante o processo
<br /><br />

        A equipe de avalia��o ouve a comunidade nas suas considera��es sobre o processo, analisa, e, caso considere procedente, corre��es no modelo proposto poder�o ser aplicadas. A execu��o deve acontecer com integridade, confiabilidade e efetividade de todo o plano tra�ado. Desnecess�rio dizer da import�ncia de haver possibilidade e flexibilidade de corre��es in loco. O objetivo de adequa��o deve ser preservado, porquanto o momento oportuno � premissa b�sica.
<br /><br />

        d) Consenso de equipe no julgamento de valor
<br /><br />

        Estudos e relat�rios dizem respeito ao momento do julgamento. As informa��es s�o analisadas, sintetizadas, comparadas e julgadas. Ser�o constru�das as proje��es e recomenda��es de aprimoramentos e transforma��es, apresentadas � comunidade avaliada, que responder� a pergunta �se valeu a pena avaliar�. � a �avalia��o da avalia��o"
<br /><br />
<strong>Fase
4:</strong> Identifica��o dos elementos operacionais e a constru��o dos indicadores para o ambiente de aplica��o da avalia��o
<br /><br />
        a) Da Avalia��o<br /><br />
        - Defini��o clara dos objetivos de avalia��o.<br />
        - Defini��o do objeto de estudo.<br />
        - Defini��o clara da abordagem na avalia��o do programa.<br />
        - Identifica��o do n�vel de consci�ncia e participa��o dos envolvidos.<br />
        - Circunst�nciasderealiza��o.<br />
        - Identifica��o dos sujeitos da pesquisa.<br />
        - Defini��o do universo e amostra.
<br /><br />


        b) Do objeto em foco (projeto, programa, institui��o)<br /><br />
        - Considera��o da hip�tese de o objeto ser inovador, ainda que n�o cumpra inicialmente seus objetivos.<br />
        - A linha de tempo da exist�ncia do objeto e o per�odo escolhido para a avalia��o.<br />
        - Defini��o das compet�ncias do objeto.
<br /><br />
        c)Do Processo<br /><br />
        - Tradu��o operacional dos impactos observados.?<br />
        - Estudos e teste de implanta��o dos meios de avalia��o.<br />
        - Pr�-difus�o do processo.<br />
        - Dimens�es da pesquisa, categorias de an�lise e constru��o de indicadores.<br />