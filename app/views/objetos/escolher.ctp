<?php
	echo $session->flash();
?>
<h3>Objetos > Escolher</h3>

<p>Esta � a primeira etapa para cadastrar uma avalia��o. Escolha um objeto previamente cadastrado para avan�ar para pr�xima etapa.

<?php
	echo $form->create('Objeto', array('action' => '/escolher/'));
	echo "<br />";
	echo $form->input('objeto_id', array('type' => 'select' ,'label' => 'Objeto:','class' => 'smallInputText', 'options' => $objeto, 'title' => 'Escolha o objeto de estudo de sua avalia��o.'));
	echo "<br />";
	echo $form->submit('Avan�ar', array('class' => 'smallSubmit', 'title' => 'Seguir para o pr�ximo passo de cria��o da avalia��o.'));
?>