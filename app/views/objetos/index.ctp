<?php
	echo $session->flash();
?>
<h3>Objetos</h3>
<p>
Clique em <i>'Novo'</i> para criar um novo <strong>objeto de estudo</strong>.<br /><br />

Na lista abaixo, podemos visualizar os <strong>objetos de estudo</strong> cadastrados no sistema.<br />
Clique no nome do objeto de estudo para visualizar os <strong>conjuntos universo</strong> cadastrados para o mesmo. <br />
</p>
<?php 

echo $form->create('Objeto', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit', 'title' => 'Cadastrar um novo objeto de estudo'));	

echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T�tulo</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach($objetos as $indice => $valor){
	echo "<tr>";
	echo "<td class='cell'>";
	echo $html->link($valor["Objeto"]["nome"],'/universos/index/'.$valor["Objeto"]["id"]);
	echo "</td>";
	echo "<td class='actionCell'>";
	echo $html->link("Editar",'/objetos/editar/'.$valor['Objeto']['id']);
	echo "</td>";
	echo "<td class='actionCell'>";
	echo $html->link("Excluir", '/objetos/excluir/'.$valor['Objeto']['id'], null, "Deseja realmente excluir este objeto?");
	echo "</td>";
	echo "</tr>";
}
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers(); 
echo $paginator->next("Pr�xima"); 
?>