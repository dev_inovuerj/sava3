<h3>Objetos > Editar</h3>

<?php
	echo $form->create('Objeto', array('action' => 'editar'));
	echo "<br />";
	echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText', 'title' => 'Nome do Objeto em estudo'));
	echo "<br />";
	echo $form->input('objeto_novo', array('type' => 'select' ,'label' => 'O objeto de estudo � novo ou rec�m criado?','class' => 'smallInputText', 'options' => array('1' => 'Sim', '0' => 'N�o'), 'title' => 'Informa��o relacionada ao tempo de exist�ncia do objeto.'));
	echo "<br />";
	echo $form->input('tempo_existencia', array('label' => 'Tempo de exist�ncia do objeto. Pequeno hist�rico de quem � ou o que � o objeto escolhido para ser avaliado:', 'class' => 'smallTextArea', 'title' => 'Pequeno hist�rico de quem � ou o que � o objeto escolhido para ser avaliado.'));
	echo "<br />";
	echo $form->submit('Salvar', array('class' => 'smallSubmit', 'title' => 'Salvar as informa��es inseridas acima'));
?>