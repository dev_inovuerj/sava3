<?php
	echo $session->flash();
?>
<h3>Objetos > Cadastrar</h3>

<?php
	echo $form->create('Objeto', array('action' => 'cadastrar'));
	echo "<br />";
	echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText', 'title' => 'O objeto em estudo poder� ser uma institui��o ou entidade (Exemplo: Cidade do Rio de Janeir), um programa (Exemplo: Crian�a esperan�a), um projeto (Exemplo: Projeto de cotistas) ou uma pessoa (Avalia��o de desempenho'));
	echo "<br />";
	echo $form->input('objeto_novo', array('type' => 'select' ,'label' => 'O objeto de estudo � novo ou rec�m criado?','class' => 'smallInputText', 'options' => array('Sim' => 'Sim', 'N�o' => 'N�o'), 'selected' => 'Sim' , 'title' => 'Informa��o relacionada ao tempo de exist�ncia do objeto.'	));
	echo "<br />";
	echo $form->input('tempo_existencia', array('label' => 'Tempo de exist�ncia do objeto. Pequeno hist�rico de quem � ou o que � o objeto escolhido para ser avaliado:', 'class' => 'smallTextArea', 'title' => 'Pequeno hist�rico de quem � ou o que � o objeto escolhido para ser avaliado.'));
	echo "<br />";
	echo $form->submit('Cadastrar', array('class' => 'smallSubmit', 'title' => 'Salvar as informa��es inseridas acima'));
?>