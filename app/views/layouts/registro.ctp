<?php header('Content-type: text/html; charset=iso-8859-1');?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br"> 
<head> 
<title>Sava3</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
<meta http-equiv="content-language" content="en" /> 
<meta http-equiv="content-language" content="pt-br" /> 
<meta name="language" content="pt-br" /> 
<meta name="author" content="Sergio Henrique" /> 
<meta name="author" content="Marcos Paulo" /> 
<meta name="DC.title" content="Sava3" /> 
<meta name="Subject" content="Internet" /> 
<meta name='description' content='Sistema de avalia��o usando a metodologia a3' /> 
<meta name='keywords' content='avalia��o, metodologia, a3, marinilza ' /> 
<meta name="classification" content="Avalia�ao" /> 
<meta name="robots" content="ALL" /> 
<meta name="distribution" content="Global" /> 
<meta name="rating" content="General" /> 
<meta name="doc-type" content="Web Page" /> 
<meta name="doc-class" content="Completed" /> 
<meta name="doc-rights" content="Copywritten Work" /> 
<meta name="revisit-after" content="4" /> 

<?php echo $html->css(array('typography','form','logado','tipTip'));   ?>
<?php echo $html->script(array('jquery-1.7.2.min','jquery.tipTip.minified')); ?>

<script>
$(window).load(function(){
	$(":input").tipTip({maxWidth: "auto", edgeOffset: 10});
});
</script>

</head> 
<body>
	<div id="wrapper"> 
    	<div id="userName">
   
        </div>
        <div id="top">
			<div id="logo">
				<?php echo $this->Html->image('logo.gif', array('alt' => 'SavA3'))?>
			</div>
        </div>
		
        <div id="middle">
            <?php echo $content_for_layout ?>
        </div>
        <div id="bottom">
			<div id="footer">
				Desenvolvido por Marcos Paulo e S�rgio Henrique.
			</div>
        </div>
    </div>
</body>
</html>