<?php header('Content-type: text/html; charset=iso-8859-1'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
    <head>
        <title>Sava3</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="content-language" content="en" />
        <meta http-equiv="content-language" content="pt-br" />
        <meta name="language" content="pt-br" />
        <meta name="author" content="Sergio Henrique" />
        <meta name="author" content="Marcos Paulo" />
        <meta name="DC.title" content="Sava3" />
        <meta name="Subject" content="Internet" />
        <meta name='description' content='Sistema de avalia��o usando a metodologia a3' />
        <meta name='keywords' content='avalia��o, metodologia, a3, marinilza ' />
        <meta name="classification" content="Avalia��o" />
        <meta name="robots" content="ALL" />
        <meta name="distribution" content="Global" />
        <meta name="rating" content="General" />
        <meta name="doc-type" content="Web Page" />
        <meta name="doc-class" content="Completed" />
        <meta name="doc-rights" content="Copywritten Work" />
        <meta name="revisit-after" content="4" />


        <?php
        # CSS
        echo $html->css(array('typography', 'form', 'logado', 'report', 'tipTip'));

        # JSTREE s� funciona com jquery vers�o 2.0.0
        echo $html->script("http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js");
        echo $this->Html->css('jstree/themes/default/style.min.css');
        echo $html->script(array('jquery.tipTip.minified.js', 'jstree.min.js'));

        $color = array(
            '#CD0000',
            '#FFB90F',
            '#76EEC6',
            '#8B4726'
        );
        ?>
    </head>
    <body>
        <div id="wrapper">
            <div id="userName">
                <div id="user">
                    <?php echo $session->read('Auth.Usuario.usuario') . "  " . $html->link($html->image('btn_logout.png', array('alt' => 'Logout', 'border' => '0')), '/usuarios/logout', array('escape' => false)); ?>
                </div>
            </div>
            <div id="top">
                <div id="logo">
                    <?php echo $this->Html->image('logo.gif', array('alt' => 'SavA3')) ?>
                </div>
                <div id="menu">
                    <ul>
                            <!-- <li><?php echo $html->link("Objeto", "/objetos/"); ?></li>  -->
                        <li><?php echo $html->link("Avalia��o", "/avaliacaos/"); ?></li>
                        <!--<li><?php echo $html->link("Usu�rio", "/usuarios/listar/"); ?></li>-->
                        <li><?php echo $html->link("Ajuda", "/ajudas/"); ?></li>
                    </ul>
                </div>
                <div class='clearFloat'></div>
            </div>

            <div id="middle">
                <?php echo $content_for_layout ?>
            </div>
            <div id="bottom">
                <div id="footer">
                    Desenvolvido por Marcos Paulo e S&eacute;rgio Henrique.
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript">

        $(window).load(function () {
            jQuery(":input , .jstree-anchor").tipTip(
                    {
                        defaultPosition : "top",
                        maxWidth: "auto",
                        edgeOffset: 5,
                        content : false
                    });
        });

        $(document).ready(function () {
            jQuery('.voltar').click(function () {
                window.location.href = $(this).attr('url');
            });
        });
    </script>
</html>