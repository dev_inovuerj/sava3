<?php

echo $session->flash();

# Avalia��o
echo "<span class='link_avaliacao' >Avalia��o: " . $session->read("Avaliacao.titulo") . "</span>";

echo "<ul><li>";
echo "Universo: " . $session->read("Universo.nome");
echo "<ul><li>";
echo $html->link("Dimens�o:", "/dimensaos/index/" . $session->read("Universo.id"));
echo $session->read("Dimensao.nome");
echo "<ul><li>";
echo $html->link("Categoria:", "/categorias/index/" . $session->read("Dimensao.id"));
echo $session->read("Categoria.nome");
echo "<ul><li>";
echo $html->link("Indicador:", "/indicadors/index/" . $session->read("Categoria.id"));
echo "<strong>" . $session->read("Indicador.nome") . "</strong>";
echo "</li></ul>";
echo "</li></ul>";
echo "</li></ul>";
echo "</li></ul>";


echo "<h3>Aspectos</h3>";

echo $form->create('Aspecto', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit'));


echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T�tulo</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach ($aspectos as $indice => $valor) {
    echo "<tr>";
    echo "<td class='cell'>";
    echo $html->link($valor["Aspecto"]["nome"], '#');
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Editar", "/aspectos/editar/" . $valor['Aspecto']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Excluir", "/aspectos/excluir/" . $valor['Aspecto']['id'], array('onclick' => 'confirm("Deseja realmente excluir o aspecto ' . $valor['Aspecto']['nome'] . ' ?")'));
    echo "</td>";
    echo "</tr>";
}
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers();
echo $paginator->next("Pr�xima");
