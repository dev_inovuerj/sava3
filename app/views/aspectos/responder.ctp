<h3>Responder</h3>
<br />
<ul class='questionario'>
    <?php
    echo $form->create('Aspecto', array('action' => '', 'class' => 'questionario'));

    foreach ($aspecto as $indice => $value) {
        $options = array();
        foreach ($value['Criterio'] as $i => $v) {
            $options[$v['id']] = $v['descricao'];
        }
        echo "<li>";
        echo $value['Aspecto']['pergunta'];
        echo "</li>";
        echo $form->radio(
                "Pergunta." . $value['Aspecto']['id'], 
                $options, 
                array('legend' => false, 'separator' => '<br />', 'class' => 'radio', 'required'=>TRUE)
            );
    }
    ?>
</ul>
    <?php
    echo "<br />";
    echo $form->submit('Responder', array('class' => 'smallSubmit'));
    