<h3>Aspectos</h3>

<?php
echo $form->create('Aspecto', array('action' => 'editar'));
echo "<br />";
echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText'));
echo "<br />";
echo $form->input('pergunta', array('type' => 'textarea', 'label' => 'Pergunta:', 'class' => 'smallTextArea'));
echo "<br />";
echo "<h3>Crit�rios</h3>";
echo $form->input('tipo_nivel_id', array('type' => 'select', 'label' => 'Tipo de n�vel do crit�rio:', 'class' => 'smallInputText', 'options' => $tipo_nivel, 'selected' => '1'));
echo "<br />";
echo $form->input('Criterio.0.descricao', array('type' => 'textarea', 'label' => 'Descri��o do crit�rio 1:', 'class' => 'smallTextArea'));
echo "<br />";
echo $form->input('Criterio.1.descricao', array('type' => 'textarea', 'label' => 'Descri��o do crit�rio 2:', 'class' => 'smallTextArea'));
echo "<br />";
echo $form->input('Criterio.2.descricao', array('type' => 'textarea', 'label' => 'Descri��o do crit�rio 3:', 'class' => 'smallTextArea'));
echo "<br />";
echo $form->input('Criterio.3.descricao', array('type' => 'textarea', 'label' => 'Descri��o do crit�rio 4:', 'class' => 'smallTextArea'));
echo "<br />";
echo $form->button('Editar', array('class' => 'smallSubmit'));
echo $form->button('Voltar', array(
    'type' => 'button',
    'class' => 'voltar smallSubmit',
    'url' => $this->Html->url("/aspectos/index/{$session->read('Indicador.id')}", true),
    'title' => "Retornar a Aspectos do Indicador {$session->read('Indicador.nome')}"
));


