<?php

echo $session->flash();

echo "<span class='link_avaliacao' >Avalia��o: " . $session->read("Avaliacao.titulo") . "</span>";

echo "<ul><li>";
echo $this->Html->link('Universo: ', '/universos/editar/' . $session->read('Universo.id')) . "<strong>" . $session->read("Universo.nome") . "</strong>";
echo "</li></ul>";

echo "<h3>Dimens�es</h3>";

echo $form->create('Dimensao', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit'));
echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T�tulo</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach ($dimensaos as $indice => $valor) :
    echo "<tr>";
    echo "<td class='cell'>";
    echo $html->link($valor["Dimensao"]["nome"], '/categorias/index/' . $valor['Dimensao']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Editar", "/dimensaos/editar/" . $valor['Dimensao']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Excluir", '/dimensaos/excluir/' . $valor['Dimensao']['id'], null, "Deseja realmente excluir esta dimens?o?");
    echo "</td>";
    echo "</tr>";
endforeach;
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers();
echo $paginator->next("Pr�xima");
