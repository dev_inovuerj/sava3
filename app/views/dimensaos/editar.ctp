<h3>Dimens�o/Cadastrar</h3>

<?php
echo $form->create('Dimensao', array('action' => 'editar'));
echo "<br />";
echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText'));
echo "<br />";
echo $form->button('Salvar', array('class' => 'smallSubmit'));
echo $form->button('Voltar', array(
    'type' => 'button',
    'class' => 'voltar smallSubmit',
//    'onClick' => 'voltar();',
    'url' => $this->Html->url("/dimensaos/index/{$session->read('Universo.id')}", true),
    'title' => "Retornar para Dimens�es do Universo {$session->read('Universo.nome')}"
));
