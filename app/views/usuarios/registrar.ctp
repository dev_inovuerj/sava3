<?php
echo $session->flash();
?>
<h3>Registro de usu�rio</h3>

<p>Preencha todas as informa��es solicitadas abaixo antes de utilizar a ferramenta.

    <?php
    echo $form->create('Usuario', array('action' => 'registrar'));
    echo "<br />";
    //echo $form->input('nome', array('label' => 'Nome Completo:', 'class' => 'smallInputText', 'size' => '120', 'title' => 'Digite seu nome completo'));
    //echo "<br />";
    //echo $form->input('email', array('label' => 'Email:', 'class' => 'smallInputText', 'size' => '60', 'title' => 'Digite email para contato'));
    //echo "<br />";
    //echo $form->input('telefone', array('label' => 'Telefone:', 'class' => 'smallInputText', 'size' => '30', 'title' => 'Digite telefone para contato'));
    //echo "<br />";
    echo $form->input('usuario', array('label' => 'Usuario:', 'class' => 'smallInputText', 'size' => '30', 'title' => 'Digite o nome de usu�rio que ir� utilizar para acessar a ferramenta. O sistema ir� verificar se o nome de usu�rio est� dispon�vel.'));
    echo "<br />";
    echo $form->input('senha', array('label' => 'Senha:', 'class' => 'smallInputText', 'type' => 'password', 'size' => '30', 'title' => 'Digite a senha que ser� utilizada'));
    echo "<br />";
    echo $form->submit('Cadastrar', array('class' => 'smallSubmit', 'title' => 'Clique aqui para efetuar o cadastro'));
    ?>