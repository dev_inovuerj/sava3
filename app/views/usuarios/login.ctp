<?php

echo $html->css(array('typography', 'form-login', 'login'));
echo $session->flash('auth');
echo $session->flash();
?>
<br />
<div id="login">
    <?php
    echo $form->create('Usuario', array('action' => 'login', 'class' => 'login'));
    echo $form->input('usuario', array('label' => 'Usu�rio', 'type' => 'text', 'class' => 'bigInputText'));
    echo "<br />";
    echo $form->input('senha', array('type' => 'password', 'class' => 'bigInputPassword'));
    echo "<br />";
    echo $form->submit('Login', array('class' => 'bigSubmit'));
    echo "<br />";
    ?>
</div>



<div id="about">
    <h3>Sobre o SavA3</h3>

    <p>O SavA3 - Sistema informatizado de Avalia��o com uso da Metodologia A3.</p>
</div>

<div class='clearFloat'></div>
<br />