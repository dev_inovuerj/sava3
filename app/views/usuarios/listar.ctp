<?php
echo $session->flash();
?>
<h3>Usu�rios</h3>
<p>
    Clique em <i>'Novo'</i> para criar um novo usu�rio.<br />
    Obtenha os dados dos usu&aacute;rios clicando em seus respectivos nomes.<br />
</p>
<?php
echo $form->create('Usuario', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit'));

echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >Usuario</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach ($usuarios as $indice => $valor) {
    echo "<tr>";
    echo "<td class='cell'>";
    echo $html->link($valor["Usuario"]["usuario"], '#');
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Editar", '#');
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Excluir", '#');
    echo "</td>";
    echo "</tr>";
}
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers();
echo $paginator->next("Pr�xima");
?>