<h3>Categoria/Editar</h3>

<?php
echo $form->create('Categoria', array('action' => 'editar'));
echo "<br />";
echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText'));
echo "<br />";
echo $form->button('Salvar', array('class' => 'smallSubmit'));
echo $form->button('Voltar', array(
    'type' => 'button',
    'class' => 'voltar smallSubmit',
//    'onClick' => 'voltar();',
    'url' => $this->Html->url("/categorias/index/{$session->read('Dimensao.id')}", true),
    'title' => "Retornar para Categorias da Dimens�o {$session->read('Dimensao.nome')}"
));
