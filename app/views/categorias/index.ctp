<?php

echo $session->flash();

echo "<span class='link_avaliacao' >Avalia��o: " . $session->read("Avaliacao.titulo") . "</span>";

echo "<ul><li>";
echo $this->Html->link('Universo: ', '/universos/editar/' . $session->read('Universo.id')) . $session->read("Universo.nome");
echo "<ul><li>";
echo $html->link("Dimens�o:", "/dimensaos/index/" . $session->read("Universo.id"));
echo "<strong> " . $session->read("Dimensao.nome") . "</strong> ";
echo "</li></ul>";
echo "</li></ul>";


echo "<h3>Categorias</h3>";

echo $form->create('Categoria', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit'));

echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T�tulo</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach ($categorias as $indice => $valor) {
    echo "<tr>";
    echo "<td class='cell'>";
    echo $html->link($valor["Categoria"]["nome"], '/indicadors/index/' . $valor['Categoria']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Editar", '/categorias/editar/' . $valor['Categoria']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Excluir", '/categorias/excluir/' . $valor['Categoria']['id'], null, "Deseja realmente excluir esta categoria?");
    echo "</td>";
    echo "</tr>";
}
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers();
echo $paginator->next("Pr�xima");
?>