<?php
	echo $session->flash();
?>
<h3>Universo > Escolher</h3>

<p>Esta � a segunda etapa para cadastrar uma avalia��o. Escolha um universo previamente cadastrado para avan�ar para pr�xima etapa.

<?php
	echo $form->create('Universo', array('action' => '/escolher/'));
	echo "<br />";
	echo $form->input('universo_id', array('type' => 'select' ,'label' => 'Universo:','class' => 'smallInputText', 'options' => $universo, 'title' => 'Escolha o conjunto universo de sua avalia��o.'));
	echo "<br />";
	echo $form->submit('Avan�ar', array('class' => 'smallSubmit', 'title' => 'Seguir para o pr�ximo passo de cria��o da avalia��o.'));
?>