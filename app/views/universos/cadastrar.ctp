<?php
	echo $session->flash();
?>
<h3>Universo > Cadastrar</h3>

<?php
	echo $form->create('Universo', array('action' => 'cadastrar'));
	echo "<br />";
	echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText', 'title' => 'O conjunto do todo onde está contida a amostra que responderá o questionário.'));
	echo "<br />";
	echo $form->input('qtd_limitante', array('label' => 'Quantidade limitante:', 'class' => 'smallInputText', 'title' => 'Quantidade total de itens em estudo em seu conjunto universo. <br /> Por exemplo, Universo = Estudantes Universitários; Quantidade Limintante = 12 000 Estudantes.'));
	echo "<br />";
	echo $form->submit('Cadastrar', array('class' => 'smallSubmit' , 'title' => 'Salvar as informações inseridas acima'));
?>