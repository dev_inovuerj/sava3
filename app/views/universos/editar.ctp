<h3>Universos > Editar</h3>

<?php
	echo $form->create('Universo', array('action' => 'editar'));
	echo "<br />";
	echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText', 'title' => 'Nome do conjunto universo.'));
	echo "<br />";
	echo $form->input('qtd_limitante', array('label' => 'Quantidade limitante:', 'class' => 'smallInputText', 'title' => 'Quantidade total de itens em estudo em seu conjunto universo. <br /> Por exemplo, Universo = Estudantes Universitários; Quantidade Limintante = 12 000 Estudantes.'));
	echo "<br />";
	echo $form->submit('Salvar', array('class' => 'smallSubmit', 'title' => 'Salvar as informações inseridas acima'));
?>