<?php
echo $session->flash();
?>
<h3>Universo</h3>
<p>
    Clique em <i>'Novo'</i> para criar um novo <strong>conjunto universo</strong>.<br /><br />

    Na lista abaixo, podemos visualizar os <strong>conjuntos universo</strong> cadastrados para o objeto de estudo anteriormente selecionado.<br />
    Clique no nome do universo de estudo para visualizar as <strong>amostras</strong> cadastradas para o mesmo. <br />
</p>
<?php
echo $form->create('Universo', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit', 'title' => 'Cadastrar um novo conjunto universo'));

echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T�tulo</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach ($universos as $indice => $valor) {
    echo "<tr>";
    echo "<td class='cell'>";
    echo $html->link($valor["Universo"]["nome"], '/amostras/index/' . $valor['Universo']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Editar", '/universos/editar/' . $valor['Universo']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Excluir", '/universos/excluir/' . $valor['Universo']['id'], null, "Deseja realmente excluir este universo?");
    echo "</td>";
    echo "</tr>";
}
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers();
echo $paginator->next("Pr�xima");
?>