<?php
$color = array(
    '#CD0000',
    '#FFB90F',
    '#76EEC6',
    '#8B4726'
);
echo $html->css(array('typography', 'form', 'logado', 'report'));
?>
<h3>Relat�rio</h3>
<br />
<br />
<h3><?php echo $avaliacao['Avaliacao']['titulo']; ?></h3>

<div class='caixaRelatorio'>
    <div class='esquerda duasColunas semBorda'>
        <h5>Tipo de Avalia��o</h5>
        <?php echo $avaliacao['TipoAvaliacao']['tipo']; ?>
    </div>
    <div class='esquerda duasColunas'>
        <h5>Respons�vel</h5>
        <?php echo $avaliacao['Avaliacao']['responsavel']; ?>
    </div>
    <div class='clearFloat'></div>
</div>

<div class='caixaRelatorio umaColuna'>
    <h5>Objetivo</h5>
    <?php echo $avaliacao['Avaliacao']['objetivo']; ?>
</div>

<div class='caixaRelatorio'>
    <div class='esquerda tresColunas semBorda'>
        <h5>Objeto em estudo</h5>
        <?php echo $avaliacao['Objeto']['nome']; ?>
    </div>

    <div class='esquerda tresColunas'>
        <h5>Objeto novo</h5>
        <?php
        if ($avaliacao['Objeto']['objeto_novo'] == 1) {
            echo "Sim";
        } else {
            echo "N�o";
        }
        ?>
    </div>

    <div class='esquerda tresColunas'>
        <h5>Tempo de exist�ncia do objeto</h5>
        <?php echo $avaliacao['Objeto']['tempo_existencia']; ?>
    </div>
    <div class='clearFloat'></div>
</div>

<div class='caixaRelatorio'>
    <div class='esquerda tresColunas semBorda'>
        <h5>Universo</h5>
        <?php echo $avaliacao['Universo']['nome']; ?>
    </div>

    <div class='esquerda tresColunas'>    
        <h5>Quantidade limitante do universo</h5>
        <?php echo $avaliacao['Universo']['qtd_limitante'] . " " . $avaliacao['Amostra']['nome']; ?>
    </div>

    <div class='esquerda tresColunas'>    
        <h5>Amostra</h5>
        <?php echo $avaliacao['Amostra']['qtd_limitante'] . " " . $avaliacao['Amostra']['nome']; ?>
    </div>
    <div class='clearFloat'></div>
</div>

<div class='caixaRelatorio'>
    <div class='esquerda duasColunas semBorda'>
        <h5>Tipo de abordagem</h5>
        <?php echo $avaliacao['TipoAbordagem']['tipo']; ?>
    </div>
    <div class='esquerda duasColunas'>
        <h5>Tipo de Resultado</h5>
        <?php echo $avaliacao['TipoResultado']['tipo']; ?>
    </div>
    <div class='clearFloat'></div>
</div>

<div class='caixaRelatorio'>
    <div class='esquerda duasColunas semBorda'>
        <h5>N�vel de Consci�ncia</h5>
        <?php echo $avaliacao['Avaliacao']['nivel_consciencia']; ?>
    </div>
    <div class='esquerda duasColunas'>
        <h5>Circunstancia de realiza��o</h5>
        <?php echo $avaliacao['Avaliacao']['circunstancia_realizacao']; ?>
    </div>
    <div class='clearFloat'></div>
</div>

<div class='caixaRelatorio umaColuna'>
    <h5>Sujeito da pesquisa</h5>
    <?php echo $avaliacao['Avaliacao']['sujeito_pesquisa']; ?>
</div>

<div class='caixaRelatorio umaColuna'>    
    <h5>Afetado pela pesquisa</h5>
    <?php echo $avaliacao['Avaliacao']['afetado']; ?>
</div>

<div class='caixaRelatorio umaColuna'>
    <h5>Agente de transforma��o</h5>
    <?php echo $avaliacao['Avaliacao']['agente_transformacao']; ?>
</div>

<div class='caixaRelatorio'>
    <div class='esquerda duasColunas semBorda'>
        <h5>Avalia��o Inovadora</h5>
        <?php echo $avaliacao['Avaliacao']['avaliacao_inovadora']; ?>
    </div>
    <div class='esquerda duasColunas'>
        <h5>Avalia��o testada</h5>
        <?php
        if ($avaliacao['Avaliacao']['testado'] == 1) {
            echo 'Sim';
        } else {
            echo 'N�o';
        }
        ?>
    </div>
    <div class='clearFloat'></div>
</div>

<div class='caixaRelatorio umaColuna'>
    <h5>Divulga��o dos resultados obtidos</h5>
    <?php echo $avaliacao['Avaliacao']['divulgacao']; ?>
</div>




<?php
/* O relat�rio */

foreach ($relatorio as $indice => $dimensao) {
    //echo "<h5>".$dimensao['Dimensao']['nome']."</h5 >";

    foreach ($dimensao['Categorias'] as $indice => $categoria) {
        //echo "<h5>".$categoria['Categoria']['nome']."</h5>";

        foreach ($categoria['Indicadores'] as $indice => $indicador) {
            echo "<div class='caixaRelatorio umaColuna'>";
            echo "<h5>" . $indicador['Indicador']['nome'] . "</h5>";

            foreach ($indicador['Aspectos'] as $indice => $aspecto) {
                //echo "O aspecto ".$aspecto['Aspecto']['nome']." obteve";
                //echo $aspecto['Respostas']['total_respostas'];
                $maior = $aspecto['Respostas']['criterios'][0]['total'];
                $nivel = $aspecto['Respostas']['criterios'][0]['Nivel'];
                $grafico = array();
                $legenda = array();
                $label = '';


                foreach ($aspecto['Respostas']['criterios'] as $indice => $resposta) {
                    $grafico[] = $resposta['total'];
                    $legenda[] = utf8_encode($resposta['Criterio']['descricao']);
                    if ($resposta['total'] > $maior) {
                        $maior = $resposta['total'];
                        $nivel = $resposta['Nivel'];
                    }
                }

//                debug($grafico);

                echo $nivel . " no aspecto " . $aspecto['Aspecto']['nome'] . ".<br /> ";
                $googleChart->setChartAttrs(
                        array(
                            'type' => 'pie',
                            'title' => '',
                            'dataLabel' => $grafico,
                            'data' => $grafico,
                            'size' => array(700, 200),
                            'color' => $color,
                            'labelsXY' => true,
                            'min' => array(0),
                            'max' => array(100),
                            'legend' => $legenda
                        )
                );
                echo $googleChart;
                echo "<br />";
            }
            echo "</div>";
        }
    }
}
?>
<?php
/* $color = array( 
  '#687e9b',
  '#c4ccd8',
  );

  $dataMultiple = array('20'=>20,'30'=>30,'50'=>50);

  $googleChart->setChartAttrs(
  array(
  'type'         => 'pie',
  'title'     => 'teste',
  'data'         => $dataMultiple,
  'size'         => array( 400, 200 ),
  'color'     => $color,
  'labelsXY'     => true,
  'min'        => array(0),
  'max'        => array(100),
  'legend'    => array('2008', '2009','2010')
  )
  );

  // Print chart
  echo $googleChart; */

