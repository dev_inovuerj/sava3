<?php
	echo $session->flash();
?>
<script>
	$('document').ready(function(){
		$('#selecionarTodos').click(function(){	
			if($('#selecionarTodos').attr('checked') == 'checked'){
				$('.checkUsuario').attr('checked', true);
			}else{
				$('.checkUsuario').attr('checked', false);
			}
		});
	});
</script>
<h3>Avalia&ccedil;&otilde;es/Associar</h3>
<p>
Clique em <i>'Novo'</i> para criar uma nova avalia&ccedil;&atilde;o.<br />
Obtenha os dados dos usu&aacute;rios clicando em seus respectivos nomes.<br />
</p>
<?php 

echo "<h4>Usu�rios Associados</h4>";
echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >Usu�rio</td>";
echo "<td class='title' colspan='4'>A��o</td>";
echo "</tr>";
if(!empty($usuarioAssociado)){
	foreach($usuarioAssociado as $indice => $valor){
		echo "<tr>";
		echo "<td class='cell'>";
		echo $html->link($valor["Usuario"]["usuario"],'#');
		echo "</td>";
		echo "<td class='actionCell'>";
		echo $html->link("Excluir","#");
		echo "</td>";
		echo "</tr>";
	}
}
echo "</table>";

echo "<br />";

echo "<h4>Usu�rios</h4>";
echo "<input type='checkbox' id='selecionarTodos' />Selecionar Tudo";
//echo $form->create('Avaliacao', array('action' => '/associar/'.$idAvaliacao.'/'.$valor['Usuario']['id']));
echo $form->create('Avaliacao', array('action' => '/associar/'.$idAvaliacao.'/'.$idUsuario));
echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >A��o</td>";
echo "<td class='title' >Usu�rio</td>";
echo "</tr>";
foreach($usuario as $indice => $valor){
	echo "<tr>";
	echo "<td class='actionCell'>";
	//echo $html->link("Associar","/avaliacaos/associar/".$idAvaliacao."/".$valor["Usuario"]["id"], null, "Deseja realmente associar este usu�rio para responder a avaliacao?");
	//echo "<input type='checkbox' name='data['AvaliacaoUsuario']['usuario_id'][]' value='".$valor["Usuario"]["id"]."'>";
	echo $form->checkbox('usuario_id', array('value' => $valor["Usuario"]["id"], 'name' => "data['Usuario']['id'][]", 'class'=>'checkUsuario'));
	echo "</td>";
	echo "<td class='cell'>";
	echo $html->link($valor["Usuario"]["usuario"],'#');
	echo "</td>";
	echo "</tr>";
}
echo "</table>";
echo "<br />";
echo $form->submit('Associar', array('class' => 'smallSubmit'));

?>