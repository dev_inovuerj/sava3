<h3>Avalia��o/Cadastrar</h3>

<?php
echo $form->create('Avaliacao', array('action' => 'editar_cabecalho'));
echo "<br />";
echo $form->input('titulo', array('label' => 'T�tulo:', 'class' => 'smallInputText', 'title' => 'Titulo identificador da sua avalia��o.'));
echo "<br />";
echo $form->input('codigo', array('label' => 'C�digo:', 'class' => 'smallInputText', 'title' => 'C�digo de identifica��o de no m�ximo 5 caracteres.'));
echo "<br />";
echo $form->input('tipo_avaliacao_id', array('type' => 'select', 'label' => 'Tipo de avalia��o:', 'class' => 'smallInputText', 'options' => $tipoAvaliacao, 'title' => 'O tipo da avalia��o � a identifica��o do tipo de objeto definido inicialmente.'));
echo "<br />";
echo $form->input('responsavel', array('label' => 'Respons�vel:', 'class' => 'smallInputText', 'title' => 'Respons�vel em realizar a avalia��o.'));
echo "<br />";
echo $form->input('objetivo', array('label' => 'Objetivo:', 'class' => 'smallTextArea', 'title' => 'Objetivo a ser alcan�ado com a avalia��o.'));
echo "<br />";
echo $form->input('tipo_abordagem_id', array('type' => 'select', 'label' => 'Tipo de abordagem:', 'class' => 'smallInputText', 'options' => $tipoAbordagem, 'title' => 'Forma de acesso ao ambiente da pesquisa com vistas ao tipo de resultado. <br/>Exemplo: Uma abordagem estrat�gica poderia suscitar um resultado social ou pol�tico.'));
echo "<br />";
echo $form->input('tipo_resultado_id', array('type' => 'select', 'label' => 'Tipo de resultado:', 'class' => 'smallInputText', 'options' => $tipoResultado, 'title' => 'Possui rela��o com o objetivo da avalia��o e definir� o tipo de indicador como resultado.'));
echo "<br />";
echo $form->input('nivel_consciencia', array('type' => 'select', 'label' => 'As pessoas envolvidas no processo de avalia��o que responder�o as quest�es:', 'class' => 'smallInputText', 'options' => array('N�o est�o interessados no processo avaliativo' => 'N�o est�o interessados no processo avaliativo', 'Est�o pouco interessados no processo avaliativo' => 'Est�o pouco interessados no processo avaliativo', 'Est�o interessados no processo avaliativo' => 'Est�o interessados no processo avaliativo'), 'title' => 'Objetiva identificar o grau de envolvimento e interesse daqueles que responder�o o question�rio.'));
echo "<br />";
echo $form->input('circunstancia_realizacao', array('type' => 'select', 'label' => 'Circunst�ncias de realiza��o:', 'class' => 'smallInputText', 'options' => array('Momento ideal' => 'Momento ideal', 'Bom' => 'Bom', 'Ruim' => 'Ruim'), 'title' => 'Qualifica��o do momento no qual � realizada a avalia��o.'));
echo "<br />";
echo $form->input('sujeito_pesquisa', array('label' => 'Sujeito da pesquisa:', 'class' => 'smallTextArea', 'title' => 'Pequena descri��o das pessoas, grupo , instiui��o ou setor para o qual est� direcionada a avalia��o.'));
echo "<br />";
echo $form->input('afetado', array('label' => 'Afetado pelo processo avaliativo:', 'class' => 'smallTextArea', 'title' => 'Pequena descri��o sobre o p�blico afetado pelo processo avaliativo.'));
echo "<br />";
echo $form->input('agente_transformacao', array('label' => 'Agente de transforma��o:', 'class' => 'smallTextArea', 'title' => 'Respons�vel por atuar sobre o resultado da avalia��o com proposta de interven��o.'));
echo "<br />";
echo $form->input('objeto_novo', array('type' => 'select', 'label' => 'Avalia��o inovadora:', 'class' => 'smallInputText', 'options' => array('1' => 'Sim', '0' => 'N�o'), 'selected' => '1', 'title' => 'Qualifica��o no tocante ao objeto em avalia��o ter a caracter�stica inovadora.'));
echo "<br />";
echo $form->input('testado', array('type' => 'select', 'label' => 'Avalia��o ser� testada antes de ser executada:', 'class' => 'smallInputText', 'options' => array('1' => 'Sim', '0' => 'N�o'), 'selected' => '1', 'title' => 'A fase de testes assegura uma melhor performance na sua aplica��o.'));
echo "<br />";
echo $form->input('divulgacao', array('label' => 'Divulga��o:', 'class' => 'smallTextArea', 'title' => 'Descri��o de como ser� feita a divulga��o de resultados da avalia��o.'));
echo "<br />";
echo $form->button('Salvar', array('class' => 'smallSubmit', 'title' => 'Salvar as informa��es inseridas acima'));
echo $form->button('Voltar', array(
    'type' => 'button',
    'class' => 'voltar smallSubmit',
//    'onClick' => 'voltar();',
    'url' => $this->Html->url("/avaliacaos/editar/{$session->read('Avaliacao.id')}", true),
    'title' => 'Retornar edi��o de avalia��o'
));

