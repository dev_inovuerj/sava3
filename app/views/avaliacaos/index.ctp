<?php echo $session->flash(); ?>
<h3>Avalia&ccedil;&otilde;es</h3>
<p>
    Clique em <i>'Novo'</i> para criar uma nova avalia&ccedil;&atilde;o.<br />
    <!--Obtenha os dados dos usuários clicando em seus respectivos nomes.<br />-->
</p>
<?php
echo $form->create('Objeto', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit', 'title' => 'Criar Nova Avalia��o'));

echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T&iacute;tulo</td>";
echo "<td class='title' colspan='4'>A&ccedil;&atilde;o</td>";
echo "</tr>";
foreach ($avaliacaos as $indice => $valor) :

    # T�tulo
    echo "<tr>";
    echo "<td class='cell'>";
    echo $html->link($valor["Avaliacao"]["titulo"], '/dimensaos/index/' . $valor["Avaliacao"]["universo_id"]);
    echo "</td>";


    /* A��es */

    # Editar
    echo "<td class='actionCell'>";
    echo $html->link("Editar", "/avaliacaos/editar/" . $valor["Avaliacao"]["id"]);
    echo "</td>";

    //echo "<td class='actionCell'>";
    //echo $html->link("Associar","/avaliacaos/associar/".$valor["Avaliacao"]["id"]);
    //echo "</td>";
    # Relat�rio
    echo "<td class='actionCell'>";
    echo $html->link("Relat�rio", "/avaliacaos/emitirRelatorio/" . $valor["Avaliacao"]["id"]);
    echo "</td>";

    # Relat�rio
    echo "<td class='actionCell'>";
    echo $this->Html->link('Estrutural', array('controller' => 'Avaliacaos', 'action' => 'emitirRelatorioEstrutural', $valor['Avaliacao']['id']));
    echo "</td>";

    # Excluir
    echo "<td class='actionCell'>";
    echo $html->link("Excluir", "/avaliacaos/excluir/" . $valor["Avaliacao"]["id"], null, "Deseja realmente excluir essa avalia&ccedil;&atilde;o?");
    echo "</td>";
    echo "</tr>";
endforeach;
echo "</table><br>";
echo $paginator->prev("Anterior");
echo "&nbsp;";
echo $paginator->numbers();
echo "&nbsp;";
echo $paginator->next("Proxima");
