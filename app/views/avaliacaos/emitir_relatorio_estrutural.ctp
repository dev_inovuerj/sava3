<style media="all">

    /* M�dia de impress�o */
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }

    /* Personalizando bot�o imprimir */
    #imprimir{
        vertical-align: super; 
        font-size: 7pt; 
        cursor: pointer;
    }

</style>

<h3>Relat�rio Estrutural 
    <span id='imprimir'  class='no-print'>
        Imprimir
    </span>
</h3>
<br />
<br />
Avalia��o:<h3><?php echo $avaliacao['Avaliacao']['titulo']; ?></h3>
Objeto:<h3><?php echo $avaliacao['Objeto']['nome']; ?></h3>
Universo:<h3><?php echo $avaliacao['Universo']['nome']; ?></h3>
Amostra:<h3><?php echo $avaliacao['Amostra']['nome']; ?></h3>

<br/>

<div id="jstree_demo_div">
    <ul>
        <?php //debug($relatorio); exit; ?>
        <?php foreach ($relatorio as $indice => $dimensao) : ?>
            <li> <?php echo $this->Html->link("Dimens�o - " . $dimensao['Dimensao']['nome'], array('controller' => 'Categorias', 'action' => 'index', $dimensao['Dimensao']['id']), array('title' => "Dimens�o - " . $dimensao['Dimensao']['nome'])); ?>
                <ul>
                    <?php foreach ($dimensao['Categorias'] as $indice => $categoria) : ?>
                        <li><?php echo $this->Html->link("Categoria - " . $categoria['Categoria']['nome'], array('controller' => 'Indicadors', 'action' => 'index', $categoria['Categoria']['id']), array('title' => "Categoria - " . $categoria['Categoria']['nome'])); ?>
                            <ul>
                                <?php foreach ($categoria['Indicadores'] as $indice => $indicador) : ?>
                                    <li><?php echo $this->Html->link("Indicador - " . $indicador['Indicador']['nome'], array('controller' => 'Aspectos', 'action' => 'index', $indicador['Indicador']['id']), array('title' => "Indicador - " . $indicador['Indicador']['nome'])); ?>
                                        <ul>
                                            <?php foreach ($indicador['Aspectos'] as $indice => $aspecto) : ?>
                                                <li>
                                                    <?php
                                                    
                                                    $array_cores = array('blue','green','white','red');
                                                    
                                                    $html_title = "<div style='text-align:left'>";
                                                    # Montando title animado com Pergunta e Crit�rios
                                                    
                                                    $html_title .= "<span style='color:#222'>Pergunta :</span><br/><span style='color:#555;'> - " . $aspecto['Aspecto']['pergunta'] . "</span><br/><br/>";
                                                    $html_title .= "<span style='color:#222'>N�veis e Crit�rios(Respostas Poss�veis):</span><br/>";
                                                    $html_title .= "<table>";
                                                    foreach ($aspecto['Respostas']['criterios'] as $ic=>$criterio) { 
                                                        $color = $array_cores[$ic];
                                                        $html_title .= "<tr>";
                                                        $html_title .= "<td><span style='color:{$color};'>".$criterio['Nivel']." </span></td>";
                                                        $html_title .= "<td><span style='color:#555;'> > " . $criterio['Criterio']['descricao'] . "</span></td>";
                                                        $html_title .= "</tr>";
                                                    };
                                                    $html_title .= "</table>";
                                                    $html_title .= "</div>";
                                                    echo $this->Html->link("Aspecto - " . $aspecto['Aspecto']['nome'], array('controller' => 'Aspectos', 'action' => 'editar', $aspecto['Aspecto']['id']), array('title' => $html_title));
                                                    ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <br/>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <br/>
                    <?php endforeach; ?>
                </ul>
            </li>
            <br/>
        <?php endforeach; ?>
    </ul>


</div>

<p>
    <?php
    echo $form->button('Voltar', array(
        'type' => 'button',
        'class' => 'voltar smallSubmit',
        'url' => $this->Html->url("/avaliacaos", true),
        'title' => "Retornar para Avalia��es"
    ));
    ?>
</p>




<script>
    /* CONFIGURA��O DO PLUGIN TREEJS */
    $(function () {

        $('.voltar').click(function () {
            window.location.href = $(this).attr('url');
        });

        // Iniciar Arvore
        $('#jstree_demo_div').jstree();
        // Abrir todos os n�s
        $('#jstree_demo_div').jstree('open_all');
        // Abre nova janela com controlador/action/id informado
        $('#jstree_demo_div').on("changed.jstree", function (e, data) {
            /* Chama nova janela com controlador informado */
            window.open(data.node.a_attr.href, "emilio");
        });

        // Bot�o Imprimir 
        $('#imprimir').click(function () {
            $('div').not("#middle,#wrapper,#jstree_demo_div").addClass('no-print');
            window.print();
            $('div').not("#middle,#wrapper,#jstree_demo_div").removeClass('no-print');
        });




//        $('#jstree_demo_div').on("changed.jstree", function (e, data) {
//            consule.log(data.selected);
//        });

//        $('button').on('click', function () {
//            $('#jstree').jstree(true).select_node('child_node_1');
//            $('#jstree').jstree('select_node', 'child_node_1');
//            $.jstree.reference('#jstree').select_node('child_node_1');
//        });

    });
</script>





<?php
/*
  <!-- Vers�o numerada -->
  <!-- <ol>
  <?php foreach ($relatorio as $indice => $dimensao) : ?>
  <li><h3>Dimens�o - <?php echo $dimensao['Dimensao']['nome']; ?></h3>
  <ol>
  <?php foreach ($dimensao['Categorias'] as $indice => $categoria) : ?>
  <li><h3>Categoria - <?php echo $categoria['Categoria']['nome']; ?></h3>
  <ol>
  <?php foreach ($categoria['Indicadores'] as $indice => $indicador) : ?>
  <li><h3>Indicador - <?php echo $indicador['Indicador']['nome']; ?></h3>
  <ol>
  <?php foreach ($indicador['Aspectos'] as $indice => $aspecto) : ?>
  <li>
  <h3>Aspecto - <?php echo $aspecto['Aspecto']['nome']; ?></h3>
  </li>
  <?php endforeach; ?>
  </ol>
  </li>
  <br/>
  <?php endforeach; ?>
  </ol>
  </li>
  <br/>
  <?php endforeach; ?>
  </ol>
  </li>
  <br/>
  <?php endforeach; ?>
  </ol>-->
 * 
 * 
 */
?>