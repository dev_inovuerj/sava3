<h3>Indicador/Editar</h3>

<?php
echo $form->create('Indicador', array('action' => 'editar'));
echo "<br />";
echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText'));
echo "<br />";
echo $form->button('Salvar', array('class' => 'smallSubmit'));
echo $form->button('Voltar', array(
    'type' => 'button',
    'class' => 'voltar smallSubmit',
//    'onClick' => 'voltar();',
    'url' => $this->Html->url("/indicadors/index/{$session->read('Categoria.id')}", true),
    'title' => "Retornar a Indicadores da Categoria {$session->read('Categoria.nome')}"
));
