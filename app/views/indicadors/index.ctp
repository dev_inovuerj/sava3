<?php

echo $session->flash();

# Avalia��o
echo "<span class='link_avaliacao'>Avalia��o: " . $session->read("Avaliacao.titulo") . "</span>";

echo "<ul><li>";
echo $this->Html->link('Universo: ', '/universos/editar/' . $session->read('Universo.id')) . $session->read("Universo.nome");
echo "<ul><li>";
echo $html->link("Dimens�o:", "/dimensaos/index/" . $session->read("Universo.id"));
echo $session->read("Dimensao.nome");
echo "<ul><li>";
echo $html->link("Categoria:", "/categorias/index/" . $session->read("Dimensao.id"));
echo "<strong>" . $session->read("Categoria.nome") . "</strong>";
echo "</li></ul>";
echo "</li></ul>";
echo "</li></ul>";

echo "<h3>Indicadores</h3>";

echo $form->create('Indicador', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit'));

echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T�tulo</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach ($indicadors as $indice => $valor) {
    echo "<tr>";
    echo "<td class='cell'>";
    echo $html->link($valor["Indicador"]["nome"], '/aspectos/index/' . $valor['Indicador']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Editar", '/indicadors/editar/' . $valor['Indicador']['id']);
    echo "</td>";
    echo "<td class='actionCell'>";
    echo $html->link("Excluir", '/indicadors/excluir/' . $valor['Indicador']['id'], null, "Deseja realmente excluir este indicador?");
    echo "</td>";
    echo "</tr>";
}
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers();
echo $paginator->next("Pr�xima");
?>