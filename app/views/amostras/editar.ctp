<h3>Amostras > Editar</h3>

<?php
	echo $form->create('Amostra', array('action' => 'editar'));
	echo "<br />";
	echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText', 'title' => 'Nome que identifica sua amostra. <br />Por exemplo, sendo Estudantes Universitários seu conjunto universo um nome identificador para amostra seria estudante.'));
	echo "<br />";
	echo $form->input('qtd_limitante', array('label' => 'Quantidade limitante:', 'class' => 'smallInputText', 'title' => 'Quantidade total do subconjunto do Universo que participará da avalização. <br />Por exemplo, sendo a quantidade limitante do conjunto universo Estudantes Universitários 12 000 estudantes, sua amostra poderia ser 7 000 estudantes.'));
	echo "<br />";
	echo $form->submit('Salvar', array('class' => 'smallSubmit', 'title' => 'Salvar as informações inseridas acima'));
?>