<?php
	echo $session->flash();
?>
<h3>Amostra > Escolher</h3>

<p>Esta � a terceira etapa para cadastrar uma avalia��o. Escolha uma amostra previamente cadastrada para avan�ar para pr�xima etapa.

<?php
	echo $form->create('Amostra', array('action' => '/escolher/'));
	echo "<br />";
	echo $form->input('amostra_id', array('type' => 'select' ,'label' => 'Amostra:','class' => 'smallInputText', 'options' => $amostra, 'title' => 'Escolha a amostra de sua avalia��o.'));
	echo "<br />";
	echo $form->submit('Avan�ar', array('class' => 'smallSubmit', 'title' => 'Seguir para o pr�ximo passo de cria��o da avalia��o.'));
?>