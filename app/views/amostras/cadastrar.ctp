<?php
	echo $session->flash();
?>
<h3>Amostras > Cadastrar</h3>

<?php
	echo $form->create('Amostra', array('action' => 'cadastrar'));
	echo "<br />";
	echo $form->input('nome', array('label' => 'Nome:', 'class' => 'smallInputText', 'title' => 'Um sub conjunto que responderá o questionário contido no universo identificado.'));
	echo "<br />";
	echo $form->input('qtd_limitante', array('label' => 'Quantidade limitante:', 'class' => 'smallInputText', 'title' => 'Quantidade total do subconjunto do Universo que participará da avaliação.'));
	echo "<br />";
	echo $form->submit('Cadastrar', array('class' => 'smallSubmit', 'title' => 'Salvar as informações inseridas acima'));
?>