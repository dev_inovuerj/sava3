<?php
	echo $session->flash();
?>
<h3>Amostras</h3>
<p>
Clique em <i>'Novo'</i> para criar uma nova <strong>amostra</strong>.<br /><br />

Na lista abaixo, podemos visualizar as <strong>amostras</strong> cadastradas para o conjunto universo anteriormente selecionado.<br />
</p>
<?php 

echo $form->create('Amostra', array('action' => '/cadastrar/'));
echo $this->Form->submit('Novo', array('class' => 'smallSubmit', 'title' => 'Cadastrar uma nova amostra'));	

echo "<table class='list'>";
echo "<tr>";
echo "<td class='title' >T�tulo</td>";
echo "<td class='title' colspan='3'>A��o</td>";
echo "</tr>";
foreach($amostras as $indice => $valor){
	echo "<tr>";
	echo "<td class='cell'>";
	echo $html->link($valor["Amostra"]["nome"],'#');
	echo "</td>";
	echo "<td class='actionCell'>";
	echo $html->link("Editar", '/amostras/editar/'.$valor['Amostra']['id']);
	echo "</td>";
	echo "<td class='actionCell'>";
	echo $html->link("Excluir", '/amostras/excluir/'.$valor['Amostra']['id'], null, "Deseja realmente excluir esta amostra?");
	echo "</td>";
	echo "</tr>";
}
echo "</table>";
echo $paginator->prev("Anterior");
echo $paginator->numbers(); 
echo $paginator->next("Pr�xima"); 
?>