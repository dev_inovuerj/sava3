<?php

class AppController extends Controller {

    var $components = array('Auth', 'Session');
    var $helpers = array('Html', 'Session', 'Javascript', 'Form', 'Paginator');
    var $permissions = array('emitirRelatorioEstrutural');

    function beforeFilter() {

        # For�ar/pegar url que usu�rio tentou logar diretamente no sistema ex.: http://www.latesi.ime.uerj.br/sava3_test/Avaliacaos/emitirRelatorioEstrutural/11
        // if ($this->params['action'] != 'login' && isset($this->params['pass'][0])) {
        //     $this->Session->write(
        //             array(
        //                 'URL_SOLICITADA' => array(
        //                     'controlador' => $this->params['controller'],
        //                     'acao' => $this->params['action'],
        //                     'valorParametro' => $this->params['pass'][0]
        //                 )
        //             )
        //     );
        // }


        
        //Security::setHash('md5');   criptografar a senha
        $this->Auth->authenticate = ClassRegistry::init('Usuario');
        $this->Auth->userModel = 'Usuario';
        $this->Auth->fields = array(
            'username' => 'usuario',
            'password' => 'senha'
        );
        $this->Auth->authorize = 'controller';
        $this->Auth->loginError = "Senha incorreta!";
        $this->Auth->authError = "Voc� n�o tem acesso a essa p�gina!";
    }

    function isAuthorized() {

        if ($this->Auth->user('grupo') == 'Administrador')
            return true; //Remove this line if you don't want admins to have access to everything by default
        if (!empty($this->permissions[$this->action])) {
            if ($this->permissions[$this->action] == '*')
                return true;
            if (in_array($this->Auth->user('grupo'), $this->permissions[$this->action]))
                return true;
        }
        return false;
    }

}
