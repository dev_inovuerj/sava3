<?php
class Universo extends AppModel{
	var $name = 'Universo';
	var $useTable = 'universo';
	
	var $hasMany = array(
        'Amostra',
		'Avaliacao'
    );
	
	var $belongsTo = array(
        'Objeto'
    ); 
}
?>