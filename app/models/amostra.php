<?php
class Amostra extends AppModel{
	var $name = 'Amostra';
	var $useTable = 'amostra';
	
	var $hasMany = array(
        'Avaliacao'
    );
	
	var $belongsTo = array(
        'Universo' 
    ); 

}
?>