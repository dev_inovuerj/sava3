<?php

class Usuario extends AppModel {

    var $name = 'Usuario';
    var $useTable = 'usuario';
    var $hasMany = array(
        'Objeto',
        'Resposta'
    );
    var $hasAndBelongsToMany = array(
        'Avaliacao' =>
        array('joinTable' => 'avaliacao_usuario',
            'foreignKey' => 'avaliacao_id',
            'associationForeignKey' => 'usuario_id'
        )
    );
    var $belongsTo = array(
        'Grupo'
    );

    function hashPasswords($data) {
        return $data;
    }

}
