<?php
class TipoNivel extends AppModel{
	var $name = 'TipoNivel';
	var $useTable = 'tipo_nivel';
	
	var $hasMany = array(
        'Criterio',
		'Nivel'
    );

}
?>