<?php
class Categoria extends AppModel{
	var $name = 'Categoria';
	var $useTable = 'categoria';
	
	var $hasMany = array(
        'Indicador' => array('foreignKey' => 'categoria_id')
    );
	
	var $belongsTo = array(
        'Dimensao' 
    ); 
}
?>