<?php
class Criterio extends AppModel{
	var $name = 'Criterio';
	var $useTable = 'criterio';
	
	var $hasMany = array(
        'Resposta'
    );
	
	var $belongsTo = array(
        'Aspecto',
		'TipoNivel',
		'Nivel'
    ); 
}
?>