<?php
class Indicador extends AppModel{
	var $name = 'Indicador';
	var $useTable = 'indicador';
	
	var $hasMany = array(
        'Aspecto'
    );
	
	var $belongsTo = array(
        'Categoria'
    ); 
}
?>