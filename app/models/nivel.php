<?php
class Nivel extends AppModel{
	var $name = 'Nivel';
	var $useTable = 'nivel';
	
	var $hasMany = array(
        'Criterio'
    );
	
	var $belongsTo = array(
        'TipoNivel' 
    ); 

}
?>