<?php
class Dimensao extends AppModel{
	var $name = 'Dimensao';
	var $useTable = 'dimensao';
	
	var $hasMany = array(
        'Categoria'
    );
	
	var $belongsTo = array(
        'Universo' 
    ); 
}
?>