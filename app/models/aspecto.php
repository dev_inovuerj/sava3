<?php

class Aspecto extends AppModel {

    var $name = 'Aspecto';
    var $useTable = 'aspecto';
    var $hasMany = array(
        'Criterio',
        'Resposta'
    );
    var $belongsTo = array(
        'Indicador'
    );

}
