<?php

class Avaliacao extends AppModel {

    var $name = 'Avaliacao';
    var $useTable = 'avaliacao';
    var $hasAndBelongsToMany = array(
        'Usuario' =>
        array('joinTable' => 'avaliacao_usuario',
            'foreignKey' => 'usuario_id',
            'associationForeignKey' => 'avaliacao_id'
        )
    );
    var $belongsTo = array(
        'Objeto',
        'TipoAvaliacao',
        'TipoAbordagem',
        'TipoResultado',
        'Universo',
        'Amostra'
    );

}